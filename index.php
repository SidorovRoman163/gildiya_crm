<?php
	// Показ ошибок (только на время разработки)
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	// ini_set('display_errors', 'Off');




	define('DS',		DIRECTORY_SEPARATOR);
	define('ROOT',		$_SERVER['DOCUMENT_ROOT'].DS);  
	define('CORE',		ROOT.'core'.DS);
	define('LIBS',		CORE.'libs'.DS);
	define('CACHE',		ROOT.'cache'.DS);
	define('MODULES',	ROOT.'modules'.DS);

	define('HOST',	$_SERVER['HTTP_HOST']);		// Используется для формирования COOKIE
	define('HTTP',	'http://'.HOST.'/');		// Используется в письмах и т.п.
	define('HAPI',	HOST);						// Хост API (форма обратной связи)


	// Подгрузка файлов для запуска приложения
	require_once CORE.'core_autoload.php';		// Функция автопогрузки файлов любых классов
	require_once CORE.'core_application.php';	// Базовый класс основного приложения

	// Запуск приложения
	core_application::start();
?>
