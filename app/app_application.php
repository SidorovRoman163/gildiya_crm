<?php
	class app_application extends core_application {
	
		public function __start() {
			
			self::$controller->__before_action();
			
			$return = call_user_func_array(array(
				self::$controller,
				self::$uri_method
			), self::$uri_params);
			
			$this->_js->assign(self::$uri_controller.'/'.self::$uri_method);
			$this->_css->assign(self::$uri_controller.'/'.self::$uri_method);
			
			self::$controller->__after_action();
			
			return $return; 
		}
	
	}
?>