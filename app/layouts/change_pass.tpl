<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>{$browser_title}</title>
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="/favicon.ico" type="image/x-icon" />
		{$css_assigned}
		{$js_assigned}
	</head>
	<body id="login">
		{$counters}
		<form id="change_pass_form" name="login" method="post" action="/admin/change_pass/{$token}/">
			<img width="100" height="100" id="logo_svg" src="/app/sources/img/logo.png" />
			{if $message}
				<div class="warning radius" style="display:block;">{$message}</div>  
			{else}
				<div class="warning radius"></div>  
				<input type="password" name="pass1" value="" placeholder="Новый пароль" id="passTxt1" class="stdTxt radius formTxt passTxt" autofocus="autofocus" />
				<input type="password" name="pass2"  value="" placeholder="Новый пароль ещё раз" id="passTxt2" class="stdTxt radius formTxt passTxt" />
				<div class="center">
					<input type="submit" class="button" id="enterBtn" value="Сменить пароль" />
				</div>
			{/if}
		</form>
	</body>
</html>