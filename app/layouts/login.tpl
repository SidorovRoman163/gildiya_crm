<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>{$browser_title}</title>
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="/favicon.ico" type="image/x-icon" />
		{$css_assigned}
		{$js_assigned}
	</head>
	<body id="login">
		{$counters}

		<form id="login_form" name="login" method="post" action="/admin/login/">
			<div class="center">
				<p>Email : <strong>roman@roman.ru</strong></p>
				<p>Password : <strong>roman</strong></p>
			</div>
			<div class="warning radius"></div>  
			<input type="text" name="email" value="" placeholder="E-mail" id="emailTxt" class="stdTxt radius formTxt" autofocus="autofocus" />
			<input type="password" name="pass"  value="" placeholder="Пароль" id="passTxt" class="stdTxt radius formTxt passTxt" />
			<a href="javascript:void(0);" id="forgot">Забыли пароль?</a>
			<input type="hidden" name="remember" id="remember" value="0" />  
			<a href="javascript:void(0);" type="checkbox" rel="remember" id="rememberChk" class="stdChk" />Запомнить меня</a>		
			<div class="center">
				<input type="submit" class="button" id="enterBtn" value="Войти" />
			</div>
     	</form>
		<form id="forgot_form" method="post" action="/admin/forgot/">
			<img width="100" height="100" id="logo_svg" src="/app/sources/img/logo.png" />
			<div class="warning radius"></div>  
			<input type="text" name="email" value="" placeholder="E-mail" id="emailTxt" class="stdTxt radius formTxt" autofocus="autofocus" />
			<a href="javascript:void(0);" id="to_login">Вернуться к форме авторизации</a>
			<div class="center">
				<input type="submit" class="button" id="forgotBtn" value="Восстановить" />
			</div>
		</form>
	</body>
</html>