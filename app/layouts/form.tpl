<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<title>{$browser_title}</title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	{$css_assigned}
	{$js_assigned}
</head>
<body>
	{$counters}
	<div id="shadow"></div>
	<div id="header">
		<div class="wrapper">
			<div id="grm">
				<img height="30" src="/app/sources/img/logo_lsd.png" id="grm_logo" />
				<div id="grm_text_rus">Dealer</div>
			</div>
			<div id="welcome">
				<a href="#" id="profilePanelBtn"><span></span></a>
				Добро пожаловать, <b>{$profile_fio}</b>
				<div id="profilePanel">
					<div id="userInfo">
						<b>{$profile_fio}</b>
						<a href="/admin/profile/">Профиль</a>
						<a href="/admin/logout/">Выйти</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="main">
		<div class="wrapper">
			<div id="lPart">
				{$menu_block}
			</div>
			<div id="main_column">
				{$notices}
				<div id="breadcrumbs">	
					<a href="/">Лента событий</a>
					{foreach $breadcrumbs as $bread}
						/ <a href="{$bread.url}">{$bread.title}</a>
					{/foreach}
				</div>
				<div id="content" style="width:736px;width:100%;">
					{$info_block}
					<div style="margin: 0 312px 0 0;">
						{$content}
					</div>
				</div>
			</div>
		</div>
	</div>
	<a id="gildiya" target="_blank" href="http://gildiya.pro/">Разработка проекта &laquo;Гильдия ПРО&raquo;</a>
	{$alert} 
</body>
</html>