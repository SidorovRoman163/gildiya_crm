<?php
	class app_controller extends core_object {
		
		protected static $layout		= 'default';	// название основного шаблона
		protected static $volume_id		= 0;			// активный ID раздела в главном меню
		protected static $module_id		= 0;			// ID текущего модуля
		protected static $admin			= array();		// данные администратора
		protected static $source_path	= '';
		protected static $check_auth	= true;			// флаг проверки авторизации
		
		private static $breadcrumbs		= array();		// хлебные крошки
		private static $title			= array();		// тег <title>
		
		/**
		 * callback-метод: Выполняется после отправки комментария в модулях реализующих комментарии,
		 * реализация необязательна, нужно перекрывать в дочерних классах
		 */
		protected function __after_send_comment($pid = 0) {
			// ...
		}
		
		/**
		 * callback-метод: Выполняется до любого действия любого контроллера,
		 * перекрывать в дочерних классах не рекомендуется, т.к. может нарушится безопасность системы
		 */
		public function __before_action() {
			
			$this->title('LADA Sport CRM');
			
			$this->_css->assign('jquery-ui.1.11.0.min');
			$this->_css->assign('common');
			$this->_css->assign('buttons');
			$this->_css->assign('pagination');
			$this->_css->assign('modal');
			$this->_css->assign('rosinter_feedback');
			$this->_css->assign('icomoon');
			$this->_css->assign('selectric');
			
			$this->_js->assign('jquery-1.11.1.min');
			$this->_js->assign('jquery-ui.1.11.0.min');
			$this->_js->assign('jquery-selectric.1.7.0.min');
			$this->_js->assign('pagination');
			$this->_js->assign('common');
			$this->_js->assign('validate');
			$this->_js->assign('combobox');
			$this->_js->assign('tabs');
			
			$this->_js->assign('tinymce/tinymce.min');
			$this->_js->assign('tinymce/settings');
			
			// ID модуля
			$sql  = 'SELECT id '.
					'FROM '.self::$pref.'_modules '.
					'WHERE name = '.$this->_sql->escape(self::$uri_controller).' '.
					'LIMIT 1';
			self::$module_id = (int)$this->_sql->get_one($sql);
			
			// ID пункта меню
			$sql  = 'SELECT id '.
					'FROM '.self::$pref.'_menu '.
					'WHERE '.$this->_sql->escape($_SERVER['REQUEST_URI']).' LIKE CONCAT(url, "%") '.
					'ORDER BY LENGTH(url) DESC, id DESC '.
					'LIMIT 1';
			self::$volume_id = (int)$this->_sql->get_one($sql);
		
			// путь к файлам
			self::$source_path	= ROOT.self::$app_name.DS.'modules'.DS.self::$uri_controller.DS.'sources'.DS;
			self::$source_path	= str_replace('dev.', '', self::$source_path);
			
			// проверка авторизации 
			if(self::$check_auth) {
				$this->check_auth(); 
			}
		}
		
		/**
		 * callback-метод: Выполняется после любого действия любого контроллера,
		 * формируются сквозные блоки и все выводится на экран
		 */
		public function __after_action() {
			
			if(self::$is_ajax) {
				die($this->_tpl->get('content'));
			}
			
			// генерируем блок главного меню
			$this->menu_controller->generate();
			
			// счётчики
			$this->_tpl->render('layouts/_counters', null, 'counters');
			
			if($this->_session->is_set('alert')) {    				  
				$alert = $this->_session->get('alert');
				$this->_tpl->assign('alert',
					'<script type="text/javascript">'.
						'$(window).load(function(){'.
							'setTimeout(function(){alert("'.$alert.'");},300);'.
						'});'.
					'</script>');
				$this->_session->delete('alert');    
			}
			
			// JS и CSS по-умолчанию
			$this->_js->assign(self::$uri_controller.'/'.self::$uri_method);
			$this->_css->assign(self::$uri_controller.'/'.self::$uri_method);
			
			// подключаем необходимые JS и CSS файлы
			$this->_tpl->assign('js_assigned', $this->_js->generate());			
			$this->_tpl->assign('css_assigned', $this->_css->generate());			
			
			// выводим все на экран
			$html = $this->_tpl->render('layouts/'.self::$layout);
			$html = str_replace('%LN%', "\r\n", $html);
			die($html);
		}
		
		/**
		 * Проверяем наличие сессии администратора,
		 * в противном случае показывам форму авторизации
		 */
		public function check_auth() {
			
			// осуществляем проверку сессии администратора
			if(self::$admin = $this->admin->cookie_check()) {
				
				// передаем данные в шаблонизатор
				$this->_tpl->assign('profile_id',			self::$admin['id']);
				$this->_tpl->assign('profile_fio',			self::$admin['fio']);
				$this->_tpl->assign('profile_email',		self::$admin['email']);
				$this->_tpl->assign('profile_phone',		self::$admin['phone']);
				$this->_tpl->assign('profile_group_id',		self::$admin['group_id']);
				
				return true;
			}
			
			$this->_js->assign('login');
			$this->_css->assign('login');
			
			// подключаем необходимые JS-файлы
			$this->_tpl->assign('js_assigned', $this->_js->generate());
			$this->_tpl->assign('css_assigned', $this->_css->generate());
			
			// отображаем форму авторизации
			echo $this->_tpl->render('layouts/login');
			die();
		}
		
		public function title($title) {
			if(!empty($title)) {
				self::$title[] = $title;
				$this->_tpl->assign('browser_title', join(' | ', array_reverse(self::$title)));
			}
		}
		
		public function breadcrumbs($title, $url) {
			if(!empty($title) && !empty($url)) {
				self::$breadcrumbs[] = array(
					'title'	=> $title,
					'url'	=> $url
				);
				$this->_tpl->assign('breadcrumbs', self::$breadcrumbs);
			}
		}
		
		public function __alert($message) {			
			$this->_session->set('alert', $message);
		}
		
		public function __filter($field, $value = null) {
			$label = join('_', array('filter', self::$app_name, self::$uri_controller, self::$uri_method, $field));
			if($value === null) {
				return $this->_session->get($label);
			}
			$this->_session->set($label, $value);
		}
	
	}
?>