<h1>{$header}</h1>
<form action="/users/save/" class="stdForm validateForm" method="post">
	<input type="hidden" name="id" value="{$id}" />
	<input type="hidden" name="entity_id" value="{$entity_id}" />
	<fieldset>
		<label class="stdFnt">Имя:</label>
		<input class="stdTxt vfNotEmpty" type="text" name="name" value="{$name}" />
	</fieldset>
	<fieldset>
		<label class="stdFnt">Статус:</label>

		<label>
			<input type="radio" class="radio" name="status" value="1" {if $status == '1'}checked{/if} />Администратор
		</label>
		<label>
			<input type="radio" class="radio" name="status" value="0" {if $status == '0'}checked{/if} />Пользователь
		</label>
	</fieldset>

	<fieldset class="buttonsBar">
		<input type="submit" class="stdButton saveButton" value="" />
		{if !empty($id)}<input type="button" class="stdButton deleteButton" rel="/users/delete/{$id}/" value="" />{/if}
	</fieldset>
</form>