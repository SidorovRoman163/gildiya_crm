{if $users_list}
	<table class="stdTbl">
		<tr>
			<th></th>
			<th class="{if $sort == 'name:asc'}asc{else}{if $sort == 'name:desc'}desc{/if}{/if}">
				<a href="javascript:void(0);" rel="name:{if $sort == 'name:asc'}desc{else}asc{/if}">Имя</a></th>
			<th class="{if $sort == 'status:asc'}asc{else}{if $sort == 'status:desc'}desc{/if}{/if}">
				<a href="javascript:void(0);" rel="status:{if $sort == 'status:asc'}desc{else}asc{/if}">Статус</a></th>
			<!-- <th>Телефоны</th> -->
			<th class="{if $sort == 'date:asc'}asc{else}{if $sort == 'date:desc'}desc{/if}{/if}">
				<a href="javascript:void(0);" rel="date:{if $sort == 'date:asc'}desc{else}asc{/if}">Дата регистрации</a></th>
		</tr>
		{foreach $users_list as $item}
			<tr class="{if $item.even()}even{else}odd{/if}">
				<td class="delButton"><a href="/users/delete/{$item.id}/"></a></td>
				<td><nobr><a href="/users/item/{$item.id}/">{$item.name}</a></nobr></td>
				<td><nobr>{$item.status}</nobr></td>
				<td width="15%"><nobr>{$item.date}</nobr></td>
			</tr>
		{/foreach}
	</table>
	{if !empty($pages_list) && count($pages_list) > 0}
		<div class="pagination">
			<a href="{$prev_link}" rel="{$prev_page}" class="prev {$prev_class}">{$lang_prev}</a>
			{foreach $pages_list as $p}
				<a href="{$p.link}" rel="{$p.num}" class="{$p.active}">{$p.page}</a>
			{/foreach}
			<a href="{$next_link}" rel="{$next_page}" class="next {$next_class}">{$lang_next}</a>
		</div>
	{/if}
{else}
	<p><br /><br /><br />Список дилеров пуст<br /><br /><br /><br /></p>
{/if}
