<h1>{$header}</h1>
<div class="buttonBar">
	<a href="/users/add/" class="button add">Добавить пользователя</a>
</div>
<div class="tableWrapper">
	<form id="filterWrapper" style="width:auto;margin-right:18px; display: none; ">
		<input type="hidden" name="page" value="{$page}" id="filterPage" />
		<input type="hidden" name="sort" value="{$sort}" id="filterSort" />
		<input type="text" name="text" value="{$text}" id="filterSearch" placeholder="Поиск по названию, сайту, телефонам и предствителю..." />
	</form>
	<div id="dealers_list">
		{$users_list}
	</div>
</div>
