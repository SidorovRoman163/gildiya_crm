<h1>{$name}</h1>
<ul class="stdTabs switch">
	<li class="first active"><a href="#common">Общее</a></li>
	<li class=""><a href="#bank_details">Реквизиты</a></li>
	<li class="last"><a href="#autos">Автомобили</a></li>
</ul>
<div id="common">
	<table class="desc">
		<tr>
			<td class="label">Имя:</td>
			<td class="value">
				{if $name}
					{$name}
				{else}
					<span>не указаны</span>
				{/if}
			</td>
		</tr>
		<tr>
			<td class="label">Статус:</td>
			<td class="value">
				{if $status}
					{$status}
				{else}
					<span>не указан</span>
				{/if}
			</td>
		</tr>
		<tr>
			<td class="label">Дата регистрации:</td>
			<td class="value">
				{if $date}
					{$date}
				{else}
				<span>не указана</span>
				{/if}
			</td>
		</tr>
		<tr>
			<td class="label"></td>
			<td class="value">
				<a class="button" href="/users/edit/{$id}/">Редактировать</a>
			</td>
		</tr>
	</table>
</div>

<div id="bank_details">
	<p>Раздел в разработке</p>
</div>

<div id="autos">
	<p>Раздел в разработке</p>
</div>
