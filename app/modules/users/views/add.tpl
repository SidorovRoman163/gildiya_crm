<h1>{$header}</h1>
<form action="/users/add_save/" class="stdForm validateForm" id="addUserForm" method="post">
	<fieldset>
		<label class="stdFnt">Имя пользователя:</label>
		<input class="stdTxt vfNotEmpty" type="text" name="name" value="" placeholder="Иван Иванович Иванов" />
	</fieldset>
	<fieldset>
		<label class="stdFnt">Статус:</label>

		<label>
			<input type="radio" class="radio" name="status" value="1" />Администратор
		</label>
		<label>
			<input type="radio" class="radio" name="status" value="0" checked />Пользователь
		</label>
	</fieldset>
	<fieldset class="buttonsBar">
		<input type="submit" class="stdButton saveButton" value="" style="display:none;" />
		<a href="javascript:void(0);" class="button">Создать нового пользователя</a>
	</fieldset>
</form>