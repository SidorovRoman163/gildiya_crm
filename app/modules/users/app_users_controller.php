<?php
	class users_controller extends app_controller {

		public static $limit = 5;

		public function index() {

			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}

			// фильтрация
			foreach(array(
						'page'			=> 0,
						'sort'			=> 'name:asc',
						'text'			=> '',
						'state_id'		=> 1
					) as $field => $default) {
				if(isset(self::$post[$field])) {
					$this->__filter($field, self::$post[$field]);
				} elseif($this->__filter($field) === null) {
					$this->__filter($field, $default);
				}
			}

			$data['sort']	= $this->__filter('sort');
			$data['page']	= (int)$this->__filter('page');
			$data['text']	= $this->__filter('text');
			$page			= $data['page'];

			$this->title('Все пользователи');
			$data['header']	= 'Все пользователи';

			// список дилеры


			$sql  = 'SELECT SQL_CALC_FOUND_ROWS DISTINCT d.* '.
				'FROM '.self::$pref.'_users d '.
				(preg_match('/^name:(asc|desc)$/', 			$this->__filter('sort'), $m)	? 'ORDER BY d.name		'.$m[1].' ' : '').
				(preg_match('/^status:(asc|desc)$/', 		$this->__filter('sort'), $m)	? 'ORDER BY d.status	'.$m[1].' ' : '').
				(preg_match('/^date:(asc|desc)$/', 			$this->__filter('sort'), $m)	? 'ORDER BY d.date		'.$m[1].' ' : '').
				'LIMIT '.((int)$page * self::$limit).', '.self::$limit;

			$data['users_list'] = $this->_sql->get_all($sql);

			if(!empty($data['users_list'])) {
				foreach($data['users_list'] as $i => &$item) {

					// статус
					if(empty($item['status'])) {
						$item['status'] = 'Пользователь';
					} else {
						$item['status'] = 'Администратор';
					}
				}
			}

			// постраничка
			$count_pages = ceil($this->_sql->get_one('SELECT FOUND_ROWS()') / self::$limit);
			if(!empty($count_pages) && $count_pages > 1) {
				$data['pages_list'] = array();
				for($i = 0; $i < $count_pages; $i++) {
					$data['pages_list'][] = array(
						'num'		=>  $i,
						'page'		=> ($i + 1),
						'link'		=> ($page == $i) ? 'javascript:void(0);': ('/dealers/'.($i > 0 ? $i.'/' : '')),
						'active'	=> ($page == $i) ? 'active' : ''
					);
				}
				$data['prev_link']	= '/dealers/'.($page - 1 > 0 ? ($page - 1).'/' : '');
				$data['next_link']	= '/dealers/'.($page + 1 > 0 ? ($page + 1).'/' : '');
				$data['prev_page']	= ($page - 1 > 0 ? ($page - 1) : '');
				$data['next_page']	= ($page + 1 > 0 ? ($page + 1) : '');
				if($page == 0) {
					$data['prev_class'] = 'inactive';
					$data['prev_link']	= 'javascript:void(0);';
				}
				if($page == $count_pages - 1) {
					$data['next_class'] = 'inactive';
					$data['next_link']	= 'javascript:void(0);';
				}
			}

			// получаем список дилеров
			$data['users_list'] = $this->_tpl->render('users/index_list', $data);

			if(self::$is_ajax) {
				die(json_encode(array(
					'state'			=> 'ok',
					'sql'			=> $sql,
					'users_list'	=> $data['users_list']
				)));
			}

			$this->_tpl->render('users/index', $data, 'content');
		}

		/**
		 * Форма создания нового дилера
		 */
		public function add() {

			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}

			$data['header']	= 'Новый пользователь';
			$this->title('Пользователи');
			$this->title($data['header']);
			$this->breadcrumbs('Пользователи', '/users/index');

			self::$volume_id = 24;
			$this->_tpl->render('users/add', $data, 'content');
		}

		/**
		 * AJAX-Метод создания нового дилера
		 */
		public function add_save() {

			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				die(json_encode(array(
					'status'	=> 'error',
					'message'	=> 'permission_denied'
				)));
			}

			// проверка входных данных
			foreach(array('name') as $field) {
				if(empty(self::$post[$field])) {
					die(json_encode(array(
						'status'	=> 'error',
						'message'	=> 'empty_field',
						'field'		=>  $field
					)));
				}
			}

			// подготавливаем и сохраняем данные дилера
			$user = array(
				'name'		=> self::$post['name'],
				'status'	=> self::$post['status']
			);
			$user_id = (int)$this->_sql->insert(self::$pref.'_users', $user);

			die(json_encode(array(
				'status'	=> 'ok',
				'redirect'	=> '/users/item/'.$user_id.'/',
			)));
		}

		/**
		 * Обзорная страница пользователя
		 */
		public function item($id = 0) {

			// получаем данные о пользователе
			$sql  = 'SELECT *'.
				'FROM '.self::$pref.'_users '.
				'WHERE id = '.(int)$id ;
			$data = $this->_sql->get_row($sql);

			if(empty($data)) {
				return $this->error_controller->page_404();
			}

			if(!empty($data)) {
				// статус
				if(empty($data['status'])) {
					$data['status'] = 'Пользователь';
				} else {
					$data['status'] = 'Администратор';
				}
			}

			// заголовки, крошки
			$this->title('Пользователь');
			$this->title($data['name']);
			$this->breadcrumbs('Пользователи', '/users/');

			// время создания
			$data['date'] = date('d.m.Y H:i:s', strtotime($data['date']));

			// рендеры
			$this->_tpl->render('users/item', $data, 'content');
		}

		public function edit($id = 0) {

			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}
			$this->title('Пользователь');
			$this->breadcrumbs('Пользователи', '/users/');

			$sql  = 'SELECT * '.
				'FROM '.self::$pref.'_users '.
				'WHERE id = '.(int)$id ;
			$data = $this->_sql->get_row($sql);

			if(!empty($data)) {
				$data['header']			= $data['name'];
				$data['title']			= htmlspecialchars($data['name']);
				$data['ownership_id']	= '0';

				$this->breadcrumbs('Обзор', '/users/item/'.$data['id'].'/');
			} else {
				$data['id']				= '0';
				$data['header']			= 'Новый пользователь';
				$data['ownership_id']	= '0';
			}

			$this->title($data['name']);
			$this->_tpl->render('users/edit', $data, 'content');
		}

		public function save($id = 0) {

			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}

			// подготавливаем и сохраняем данные
			$data = array(
				'name'		=> self::$post['name'],
				'status'	=> self::$post['status']
			);
			if(empty(self::$post['id'])) {
				$id = $this->_sql->insert(self::$pref.'_users', $data);
			} else {
				$id = (int)self::$post['id'];
				$this->_sql->update(self::$pref.'_users', $data, $id);
			}

			$this->__alert('Данные были успешно сохранены');
			$this->_url->redirect('/users/item/'.$id.'/');
		}

		/**
		 * Метод удаления пользователя
		 */
		public function delete($id = 0) {

			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}

			// удаляем пользователя
			$this->_sql->delete(self::$pref.'_users', (int)$id);

			$this->_url->redirect('/users/');
		}

		public function admin()
		{
			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}

			// фильтрация
			foreach(array(
						'page'			=> 0,
						'sort'			=> 'name:asc',
						'text'			=> '',
						'state_id'		=> 1
					) as $field => $default) {
				if(isset(self::$post[$field])) {
					$this->__filter($field, self::$post[$field]);
				} elseif($this->__filter($field) === null) {
					$this->__filter($field, $default);
				}
			}

			$data['sort']	= $this->__filter('sort');
			$data['page']	= (int)$this->__filter('page');
			$data['text']	= $this->__filter('text');
			$page			= $data['page'];

			$this->title('Администраторы');
			$data['header']	= 'Администраторы';

			// список дилеры
			$sql  = 'SELECT SQL_CALC_FOUND_ROWS DISTINCT d.*'.
				'FROM '.self::$pref.'_users d '.
				'WHERE d.status = 1 '.
				(preg_match('/^name:(asc|desc)$/', 			$this->__filter('sort'), $m)	? 'ORDER BY d.name		'.$m[1].' ' : '').
				(preg_match('/^status:(asc|desc)$/', 		$this->__filter('sort'), $m)	? 'ORDER BY d.status	'.$m[1].' ' : '').
				(preg_match('/^date:(asc|desc)$/', 			$this->__filter('sort'), $m)	? 'ORDER BY d.date		'.$m[1].' ' : '').
				'LIMIT '.((int)$page * self::$limit).', '.self::$limit;

			$data['users_list'] = $this->_sql->get_all($sql);

			if(!empty($data['users_list'])) {
				foreach($data['users_list'] as $i => &$item) {
					$item['status'] = 'Администратор';
				}
			}

			// постраничка
			$count_pages = ceil($this->_sql->get_one('SELECT FOUND_ROWS()') / self::$limit);
			if(!empty($count_pages) && $count_pages > 1) {
				$data['pages_list'] = array();
				for($i = 0; $i < $count_pages; $i++) {
					$data['pages_list'][] = array(
						'num'		=>  $i,
						'page'		=> ($i + 1),
						'link'		=> ($page == $i) ? 'javascript:void(0);': ('/dealers/'.($i > 0 ? $i.'/' : '')),
						'active'	=> ($page == $i) ? 'active' : ''
					);
				}
				$data['prev_link']	= '/dealers/'.($page - 1 > 0 ? ($page - 1).'/' : '');
				$data['next_link']	= '/dealers/'.($page + 1 > 0 ? ($page + 1).'/' : '');
				$data['prev_page']	= ($page - 1 > 0 ? ($page - 1) : '');
				$data['next_page']	= ($page + 1 > 0 ? ($page + 1) : '');
				if($page == 0) {
					$data['prev_class'] = 'inactive';
					$data['prev_link']	= 'javascript:void(0);';
				}
				if($page == $count_pages - 1) {
					$data['next_class'] = 'inactive';
					$data['next_link']	= 'javascript:void(0);';
				}
			}

			// получаем список дилеров
			$data['users_list'] = $this->_tpl->render('users/index_list', $data);

			if(self::$is_ajax) {
				die(json_encode(array(
					'state'			=> 'ok',
					'sql'			=> $sql,
					'users_list'	=> $data['users_list']
				)));
			}

			$this->_tpl->render('users/index', $data, 'content');
		}
	}
?>