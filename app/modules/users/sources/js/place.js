(function($) {
	
	var myMap;
	var myPlacemark;
	
	ymaps.ready(function() {
		var lat		= $('#latitudeField').val() || 51.821929;
		var lng		= $('#longitudeField').val() || 107.630053;
		var zoom	= $('#zoomField').val() || 13;
		
        myMap = new ymaps.Map('map_block', {center: [lat, lng], zoom: zoom});
		myMap.controls.add(new ymaps.control.ZoomControl());
		
		myPlacemark = new ymaps.Placemark([lat, lng], {}, {
			draggable: true,
            iconImageHref: '/app/sources/img/marker.png',
            iconImageSize: [28, 42],
            iconImageOffset: [-14, -42]
        });
        myMap.geoObjects.add(myPlacemark);
		
		myPlacemark.events.add('dragend', function(e) {
			var pos = e.get('target').geometry.getCoordinates();
			$('#latitudeField').val(pos[0]);
			$('#longitudeField').val(pos[1]);
		});
		
		myMap.events.add('click', function(e) {
			var pos = e.get('coordPosition');
			myPlacemark.geometry.setCoordinates(pos);
			
			$('#latitudeField').val(pos[0]);
			$('#longitudeField').val(pos[1]);
		});
		
		myMap.events.add('actiontickcomplete', function(event) {
			var center	= event.get('tick').globalPixelCenter;
			var zoom	= event.get('tick').zoom;
			
			$('#latitudeField').val(myMap.getCenter()[0]);
			$('#longitudeField').val(myMap.getCenter()[1]);
			$('#zoomField').val(zoom);
		});
		
		/**
		 * Свободный поиск по карте
		 */
		$('#searchGo').click(function() {
			var search = $('#searchField').val();
			if(!search) {
				return false;
			}
			
			ymaps.geocode(search, {
				results: 1
			}).then(function(res) {
				var geoObject	= res.geoObjects.get(0),
					coords		= geoObject.geometry.getCoordinates(),
					bounds		= geoObject.properties.get('boundedBy');
				
				myPlacemark.geometry.setCoordinates(coords);
				myMap.setCenter(coords);
				$('#latitudeField').val(coords[0]);
				$('#longitudeField').val(coords[1]);
			});
			
			return false;
		});
		
	});
	
	
	$(document).ready(function() {
		
		$('#feddistField').change(function() {
			$('#areaField').attr('disabled', 'disabled');
			$('#areaField').selectric('init');
			$.getJSON('/geo/get_areas/'+$(this).val()+'/', function(r) {
				if(r.length > 0) {
					$('#areaField').html('<option value="0"></option>');
					for(var i in r) {
						$('#areaField').append('<option value="'+r[i].id+'">'+r[i].title+'</option>');
					}
					$('#areaField').removeAttr('disabled');
					$('#areaField').selectric('init');
				}
			});
		});
		
	});

})(jQuery)