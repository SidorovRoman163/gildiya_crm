(function($) {
	
	/**
	 * Кнопка отправки формы
	 */
	$(document).on('click', '#addUserForm .buttonsBar .button', function() {
		$('#addUserForm').submit();
		return false;
	});
	
	/**
	 * Обработчик отправки формы
	 */
	$(document).ready(function() {

		$(document).on('submit', '#addUserForm', function() {
			if(window.has_errors) return false;
			
			$.ajax({
				url:		$(this).attr('action'),
				type:		$(this).attr('method'),
				data:		$(this).serialize(),
				dataType:	'json',
				success:	function(r) {
					if(r.status == 'ok') {
						location.href = r.redirect;
					}
				}
			});
			return false;
		});

	});

})(jQuery);