(function($) {

    $(document).ready(function() {

        if($('#filterWrapper').length == 1) {
            $('#filterWrapper').data('ajaxDataType', 'json');
            $('#filterWrapper').data('ajaxSuccess', function(data) {
                if(data.state == 'ok') {
                    $('#dealers_list').html(data.users_list);
                }
                return false;
            });
        }

    });

})(jQuery)