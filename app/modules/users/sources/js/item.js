(function($) {
	
	$(document).ready(function() {
		
		/**
		 * Кнопка "Добавить" новое местоположение дилера
		 */
		$('#addPlaceButton').click(function() {
			
			$('#dealer_item_popup').dialog({
				width: 446,
				modal: true
			});
			
			return false;
		});
		
	});

})(jQuery)