<?php
	class admin_group_model extends core_model {
		
		protected function __init_model() {
			
			// определяем структуру данных модели
			$this->struct = array(
				'title'	=> array(
					'type'		=> self::T_STRING,
					'length'	=> 50,
					'valid'		=> self::V_NOT_EMPTY,
				),
			);
			
		}
		
		protected function __after_get($data) {
			if(!empty($data['id'])) {
				$sql =  'SELECT title FROM '.$this->get_table('admin_group_lang_title').' '.
						'WHERE group_id = '.(int)$data['id'].' AND lang_id = '.(int)self::$lang_id;
				$data['title'] = $this->_sql->get_one($sql);
			}
			return $data;
		}
	
	}
?>