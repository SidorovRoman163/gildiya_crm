<?php
	class admin_controller extends app_controller {
		
		public static $limit	= null;
		
		public function __init() {
			self::$volume_id 	= 2;
			self::$limit		= 10;
		}
		
		public function __before_action() {
			
			// отмена проверки авторизации для некоторых методов
			if(in_array(self::$uri_method, array('login', 'forgot', 'change_pass'), true)) {
				self::$check_auth = false;
			}
			
			parent::__before_action();
		}
		
		public function index() {
			
			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}
			
			// фильтрация
			foreach(array(
				'group_id'		=> 0,
				'page'			=> 0,
				'sort'			=> 'fio:asc',
			) as $field => $default) {
				if(isset(self::$post[$field])) {
					$this->__filter($field, self::$post[$field]);
				} elseif($this->__filter($field) === null) {
					$this->__filter($field, $default);
				}
			}
			
			$this->title('Пользователи');
			
			// список пользователей
			$data['admins_list'] = $this->_sql->get_all(
				'SELECT a.id, a.fio, a.email, a.phone, a.is_active, '.
					'ag.title admin_group_title, d.id dealer_id, d.title dealer_title '.
				'FROM '.self::$pref.'_admin a '.
					'LEFT JOIN '.self::$pref.'_admin_group ag	ON a.group_id	= ag.id '.
					'LEFT JOIN '.self::$pref.'_dealers d		ON a.id			= d.admin_id '.
				'WHERE a.is_delete = 0 '.
					($this->__filter('group_id') ? 'AND ag.id = '.(int)$this->__filter('group_id').' '		: '').
				'GROUP BY a.id '.
				(preg_match('/^email:(asc|desc)$/', 	$this->__filter('sort'), $m)	? 'ORDER BY a.email  	'.$m[1].' ' : '').	
				(preg_match('/^phone:(asc|desc)$/', 	$this->__filter('sort'), $m)	? 'ORDER BY a.phone  	'.$m[1].' ' : '').	
				(preg_match('/^fio:(asc|desc)$/', 		$this->__filter('sort'), $m)	? 'ORDER BY a.fio  		'.$m[1].' ' : '').	
				(preg_match('/^group_id:(asc|desc)$/',	$this->__filter('sort'), $m)	? 'ORDER BY ag.title 	'.$m[1].' ' : '').	
				'LIMIT '.((int)self::$limit * (int)$this->__filter('page')).', '.(int)self::$limit
			);
			if(!empty($data['admins_list'])) {
				foreach($data['admins_list'] as $i => &$item) {
					$item['phone'] = $this->_format->phone($item['phone']);
				}
			}
			
			// постраничка
			$admin_count = (int)$this->_sql->get_one(
				'SELECT COUNT(*) '.
				'FROM '.self::$pref.'_admin a '.
					($this->__filter('group_id') ? 'LEFT JOIN '.self::$pref.'_admin_group ag ON a.group_id = ag.id ' : '').
				'WHERE a.is_delete = 0 '.
					($this->__filter('group_id') ? 'AND ag.id = '.(int)$this->__filter('group_id').' '	: '')
			);
			$count_pages = ceil($admin_count / self::$limit);
			if(!empty($count_pages) && $count_pages > 1) {
				$data['pages_list'] = array();
				for($i = 0; $i < $count_pages; $i++) {
					$data['pages_list'][] = array(
						'num'		=> ($i + 1), 
						'page'		=>  $i,
						'active'	=> ($this->__filter('page') == $i) ? 'active' : ''
					);
				}
				$data['prev_page']	= ($this->__filter('page') - 1 > 0 ? ($this->__filter('page') - 1) : '0');
				$data['next_page']	= ($this->__filter('page') + 1 > 0 ? ($this->__filter('page') + 1) : '0');
				if($this->__filter('page') == 0) {
					$data['prev_class'] = 'inactive';
					$data['prev_page']	= '0';
				}
				if($this->__filter('page') == $count_pages - 1) {
					$data['next_class'] = 'inactive';
					$data['next_page']	= '0';
				}
			}
			
			// список групп
			$data['group_list'] = $this->_sql->get_all(
				'SELECT ag.id, ag.title, COUNT(ag.id) count '.
				'FROM '.self::$pref.'_admin a '.
					'LEFT JOIN '.self::$pref.'_admin_group ag ON a.group_id = ag.id '.
				'WHERE ag.is_delete = 0 AND a.is_delete = 0 '.
				'GROUP BY ag.id '.
				'ORDER BY ag.title'
			);
			if(!empty($data['group_list']) && $this->__filter('group_id')) {
				foreach($data['group_list'] as $i => &$item) {
					if($item['id'] == $this->__filter('group_id')) {
						$item['selected'] = 'selected="selected"';
					}
				}
			}
			
			$data['sort']		= $this->__filter('sort');
			$data['page']		= (int)$this->__filter('page');
			$data['group_id']	= (int)$this->__filter('group_id');
			
			$this->_tpl->render('admin/index', $data, 'content');
		}
		
		/**
		 * Обзорная страница профиля
		 */
		public function item($id = 0) {
		
			$sql  = 'SELECT a.*, ag.title group_title '.
					'FROM '.self::$pref.'_admin a '.
						'LEFT JOIN '.self::$pref.'_admin_group ag ON a.group_id = ag.id '.
					'WHERE a.id = '.(int)$id.' AND a.is_delete = 0';
			$data = $this->_sql->get_row($sql);
			if(empty($data)) {
				$this->error_controller->page_404();
			}
			
			// форматируем телефон
			if(!empty($data['phone'])) {
				$data['phone'] = $this->_format->phone($data['phone']);
			}
			
			// форматируем e-mail
			if(!empty($data['email'])) {
				$data['email'] = explode(',', $data['email']);
				foreach($data['email'] as $i => $email) {
					$email = trim($email);
					if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
						unset($data['email'][$i]);
						continue;
					} 
					$data['email'][$i] = '<a href="mailto:'.trim($email).'">'.trim($email).'</a>';
				}
				$data['email'] = join(', ', $data['email']);
			}
			
			// день рождения
			// if(strtotime($data['birthday']) > 0) {
				// $data['birthday_format'] = date('d.m.Y', strtotime($data['birthday']));
			// }
			
			$this->title('Пользователи');
			$this->title($data['fio']);
			$this->breadcrumbs('Пользователи', '/admin/');
			
			$this->_tpl->render('admin/item', $data, 'content');
		}
		
		public function add() {
			$this->edit();
		}
		
		public function edit($id = 0) {
			
			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}
			
			// обозначиваем активный раздел
			self::$layout = 'form';
			$this->breadcrumbs('Пользователи', '/admin/');
			
			$sql  = 'SELECT a.* '.
					'FROM '.self::$pref.'_admin a '.
					'WHERE a.id = '.(int)$id;
			$data = $this->_sql->get_row($sql);
			if(!empty($data)) {
				if(!empty($data['is_delete'])) {
					return $this->error_controller->page_404();
				}
				$data['header']	= (!empty($data['fio'])) ? $data['fio'] : $data['email'];
				$this->breadcrumbs($data['header'], '/admin/item/'.$data['id'].'/');
				
				// время создания
				$data['insert_time_format'] = date('d.m.Y H:i:s', strtotime($data['insert_time']));
				
				$this->_tpl->render('admin/edit_info_block', $data, 'info_block');
			} else {
				$data['id']			= '0';
				$data['lang_id']	= '0';
				$data['group_id']	= '0';
				$data['header']		= 'Новый пользователь';
			}
			
			$this->title('Пользователи');
			$this->title($data['header']);
			
			// список групп пользователей
			$sql  = 'SELECT ag.id, ag.title, IF(ag.id = '.(int)$data['group_id'].', 1, 0) selected '.
					'FROM '.self::$pref.'_admin_group ag '.
					'WHERE ag.is_delete = 0 '.
					'ORDER BY ag.title';
			$data['admin_group_list'] = $this->_sql->get_all($sql);
			
			$this->_js->assign('datapicker');
			$this->_tpl->render('admin/edit', $data, 'content');
		}
		
		public function save() {
			
			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}
		
			if(!empty(self::$post)) {
				$admin = self::$post;
				if(isset($admin['pass']) && empty($admin['pass'])) {
					unset($admin['pass']);
				}
				if($this->admin->save($admin)) {
					$this->__alert('Данные были успешно сохранены');
					$this->_url->redirect('/admin/item/'.$this->admin->id.'/');
				}
			}
			$this->_url->redirect('/admin/');
		}
		
		public function profile_save() {
			
			if(!empty(self::$post)) {
				$admin['id'] = self::$admin['id'];
				
				if(isset(self::$post['fio']))	$admin['fio']	= self::$post['fio'];
				if(isset(self::$post['phone']))	$admin['phone']	= self::$post['phone'];
				
				if(isset(self::$post['pass']) && !empty(self::$post['pass'])) {
					if(!empty(self::$post['pass2']) && self::$post['pass'] === self::$post['pass2']) {
						$admin['pass'] = self::$post['pass'];
					} else {
						$this->__alert('Ошибка: пароль не совпадает с подтверждением');
						$this->_url->referer();
					}
				}
				
				if($this->admin->save($admin)) {
					if(!empty($admin['pass'])) {
						$admin['pass'] = $this->admin->get_pass($admin['id']);
						$this->admin->cookie_set($admin['id'], $admin['pass'], 0);
						$this->__alert('Пароль был успешно изменён');
						$this->_url->redirect('/admin/profile/');
					}
					$this->__alert('Данные были успешно изменены');
					$this->_url->redirect('/admin/profile/');
				}
				
			}
			$this->_url->redirect('/');
		}
		
		public function delete($id = 0) {
			
			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}
			
			$this->admin->delete($id);
			$this->_url->redirect('/admin/');
		}
		
		/**
		 * AJAX-Метод проверки логина и пароля
		 * с последующей авторизацией
		 */
		public function login() {

			// проверка на наличие входящих данных
			foreach(array('email', 'pass') as $field) {
				if(empty(self::$post[$field])) {
					die(json_encode(array(
						'status'	=> 'error',
						'message'	=> 'empty_'.$field
					)));
				}
			}
			
			// осуществляем проверку данных
			$sql  = 'SELECT a.id, a.pass '.
					'FROM '.self::$pref.'_admin a '.
					'WHERE '.
						'a.email		= '.$this->_sql->escape(self::$post['email']).' AND '.
						'a.pass			= '.$this->_sql->escape($this->admin->getSecurePass(trim(self::$post['pass']))).' AND '.
						'a.is_active	= 1';
			$admin = $this->_sql->get_row($sql);
			if(empty($admin)) {
				die(json_encode(array(
					'status'	=> 'error',
					'message'	=> 'not_found'
				)));
			}
			
			// обновляем время последнего входа
			$this->_sql->update(self::$pref.'_admin', array(
				'last_login_time' => date('Y-m-d H:i:s')
			), (int)$admin['id']);
			
			// авторизуем
			$cookie_time = (!empty(self::$post['remember'])) ? 'year' : 0;    
			$this->admin->cookie_set($admin['id'], $admin['pass'], $cookie_time);

			die(json_encode(array(
				'status' => 'ok'
			)));
		}
		
		/**
		 * Метод проверки логина и пароля администратора
		 * для авторизации его в системе
		 */
		public function logout() {
			$pattern = $this->_conf->get('admin/pattern_cookie');
			$this->_cookie->delete_with_pattern($pattern);
			// session_destroy();
			$this->_url->redirect('/');
		}
		
		/**
		 * AJAX-Метод проверки почты и восстановления пароля
		 * с последующей отправкой письма для смены пароля
		 */
		public function forgot() {
			
			// проверка на наличие поля e-mail
			if(empty(self::$post['email'])) {
				die(json_encode(array(
					'status'	=> 'error',
					'message'	=> 'empty_email'
				)));
			}
			
			// поиск пользователя в БД
			$sql  = 'SELECT id, fio, email, pass, phone, update_time '.
					'FROM '.self::$pref.'_admin '.
					'WHERE email = '.$this->_sql->escape(self::$post['email']).' '.
					'ORDER BY id DESC '.
					'LIMIT 1';
			$admin = $this->_sql->get_row($sql);
			if(empty($admin)) {
				die(json_encode(array(
					'status'	=> 'error',
					'message'	=> 'user_not_found'
				)));
			}
			
			// формируем хеш для восстановления пароля
			$hash = hash('md5', join(':', $admin));
			
			// формируем бланк письма
			$blank = $this->settings_blank->get_blank(2, array(
				'fio'		=> $admin['fio'],
				'link'		=> HTTP.'admin/change_pass/'.$admin['id'].':'.$hash.'/',
			));
			
			// отправляем письмо
			if(!$this->_mail->send_mail($admin['email'], $blank['text'], $blank['subject'])) {
				die(json_encode(array(
					'status'	=> 'error',
					'message'	=> 'send_mail_error'
				)));
			}
			
			die(json_encode(array(
				'status' => 'ok'
			)));
		}
		
		/**
		 * Форма смены пароля
		 */
		public function change_pass($token = '') {
			
			// разбор токена
			if(!preg_match('/^(?P<id>[0-9]+)\:(?P<hash>[a-z0-9]+)$/i', $token, $match)) {
				$data['message'] = 'Неверный формат ссылки';
			} else {
				
				// получаем данные об админе
				$sql  = 'SELECT id, fio, email, pass, phone, update_time '.
						'FROM '.self::$pref.'_admin '.
						'WHERE id = '.(int)$match['id'];
				$admin = $this->_sql->get_row($sql);
				if(empty($admin)) {
					$data['message'] = 'Такого пользователя не существует';
				} else {
				
					// сверка хешей
					if($match['hash'] !== hash('md5', join(':', $admin))) {
						$data['message'] = 'Ссылка для смены пароля устарела';
					} else {
					
						// передаем токен в шаблонизатор
						$data['token'] = $token;
						
						// обработка смены пароля
						if(!empty(self::$post)) {
							
							// проверка на заполнение
							if(empty(self::$post['pass1']) || empty(self::$post['pass2'])) {
								die(json_encode(array(
									'status'	=> 'error',
									'message'	=> 'Необходимо заполнить оба поля'
								)));
							}
							
							// проверка на равнозначность
							if(self::$post['pass1'] !== self::$post['pass2']) {
								die(json_encode(array(
									'status'	=> 'error',
									'message'	=> 'Пароли не совпадают'
								)));
							}
							
							// смена пароля
							$this->admin->save(array(
								'id'		=> (int)$match['id'],
								'pass'		=> self::$post['pass1'],
							));
							
							die(json_encode(array(
								'status' => 'ok'
							)));
						}
					}
				}
			}
			
			$this->_js->assign('login');
			$this->_css->assign('login');
			
			// подключаем необходимые JS и CSS файлы
			$this->_tpl->assign('js_assigned', $this->_js->generate());
			$this->_tpl->assign('css_assigned', $this->_css->generate());
			
			echo $this->_tpl->render('layouts/change_pass', $data);
			die();
		}
		
		public function profile() {
			
			$this->title('Личный кабинет');
			
			$data = array(
				'id'	=> self::$admin['id'],
				'fio'	=> self::$admin['fio'],
				'email'	=> self::$admin['email'],
				'phone'	=> self::$admin['phone']
			);
			
			$this->_tpl->render('admin/profile', $data, 'content');
		}
		
		public function invate($id = 0) {
		
			// проверка прав пользователя
			if(!$this->admin->hasRight(self::$admin, 1)) {
				return $this->error_controller->page_403();
			}
			
			$data = $this->admin->get_item($id);
			if(empty($data)) {
				$this->__alert('Ошибка: Пользователь не обнаружен');
				$this->_url->referer();
			}
			
			if(empty($data['email'])) {
				$this->__alert('Ошибка: Невозможно активировать пользователя '.$data['fio'].' по причине неуказанного e-mail адреса');
				$this->_url->referer();
			}
			
			// назначаем отправителя
			$sql  = 'SELECT es.* '.
					'FROM '.self::$pref.'_email_senders es '.
					'WHERE es.id = 4';
			$sender = $this->_sql->get_row($sql);
			if(!empty($sender)) {
				$this->_mail->smtp($sender['smtp_host'], $sender['smtp_port'], $sender['smtp_login'], $sender['smtp_pass'], $sender['smtp_ssl']);
				$this->_mail->from($sender['email'], $sender['title']);
			}
			
			$pass = $this->admin->generate_pass();
			$blank = $this->settings_blank->get_blank(1, array(
				'fio'		=> $data['fio'],
				'email'		=> str_replace(', ', ' или ', $data['email']),
				'password'	=> $pass
			));
			
			if(!$this->_mail->send_mail($data['email'], $blank['text'], $blank['subject'])) {
				$this->__alert('Ошибка: Произошел сбой при отправлении сообщения на адрес '.$data['email']);
				$this->_url->referer();
			}
			
			$this->admin->save(array(
				'id'		=> $data['id'],
				'pass'		=> $pass,
				'is_invate'	=> 1,
				'is_active'	=> 1
			));
			
			$this->__alert('Пользователю было успешно выслано письмо с приглашением');
			$this->_url->referer();
		}
		
		public function load_admins() {
			$sql  = 'SELECT a.id, a.fio, a.post '.
					'FROM '.self::$pref.'_admin a '.
					'WHERE a.is_delete = 0 '.
						(!empty(self::$get['q']) ? 'AND a.fio LIKE '.$this->_sql->escape('%'.self::$get['q'].'%') : '').' '.
					'ORDER BY a.fio';
			$data['list'] = $this->_sql->get_all($sql);
			if(!empty($data['list'])) {
				foreach($data['list'] as $i => &$item) {
				
					// аватар 
					if($this->_file->image_exists('admin/avatars/s32', $item['id'])) {
						$item['avatar'] = $this->_file->image_tag(32, 32);
					} else {
						$item['avatar'] = '<img width="32" height="32" src="/app/sources/img/default_admin_32.png" />';
					}
					
				}
			}
			
			die(json_encode($data));
		}
	}
?>