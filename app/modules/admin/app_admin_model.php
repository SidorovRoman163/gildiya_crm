<?php
	class admin_model extends core_model {
		
		protected $salt_table	= null;
		protected $salt_cookie 	= null;
		
		protected function __init_model() {
			
			// определяем структуру данных модели
			$this->struct = array(
				'login'	=> array(
					'type'		=> self::T_STRING,
					'length'	=> 50,
					'valid'		=> self::V_NOT_EMPTY,
				),
				'pass'	=> array(
					'type'		=> self::T_STRING,
					'length'	=> 32,
					'valid'		=> self::V_NOT_EMPTY,
				),
				'fio'	=> array(
					'type'		=> self::T_STRING,
					'length'	=> 100,
				),
				'group_id'	=> array(
					'type'		=> self::T_INT,
					'length'	=> 100,
				),
				'phone'	=> array(
					'type'		=> self::T_STRING,
					'length'	=> 10,
				),
				'email'	=> array(
					'type'		=> self::T_STRING,
					'length'	=> 100,
				),
				'is_active'	=> array(
					'type'		=> self::T_INT,
				),
				'is_invate'	=> array(
					'type'		=> self::T_INT,
				),
				'last_login_time'	=> array(
					'type'		=> self::T_DATE,
				),
				'last_check_time'	=> array(
					'type'		=> self::T_DATE,
				)
			);
			
			// определяем отношения
			$this->relations = array();
			
			// определяем "соль" пароля
			$this->salt_table	= $this->_conf->get('admin/salt_table');
			$this->salt_cookie	= $this->_conf->get('admin/salt_cookie');
		
		}
		
		public function getSecurePass($pass) {
			return hash('md5', hash('md5', $this->salt_table.$pass).$this->salt_table);
		}
		
		protected function __before_get($data) {
			if(isset($data['pass'])) {
				$data['pass'] = hash('md5', hash('md5', $this->salt_table.$data['pass']).$this->salt_table);
			}
			return $data;
		}
		
		protected function __after_get($data) {
			if(isset($data['phone'])) {
				if(!empty($data['phone']) && strlen($data['phone']) == 10) {
					$data['phone'] = '+7 ('.substr($data['phone'],0,3).') '.substr($data['phone'],3,3).'-'.substr($data['phone'],6,2).'-'.substr($data['phone'],8,2);
				} else {
					$data['phone'] = '';
				}
			}
			if(isset($data['insert_time'])) {
				$data['insert_time_format'] = date('d.m.Y H:i:s', strtotime($data['insert_time']));
			}
			if(isset($data['birthday'])) {
				$data['birthday_format'] = date('d.m.Y', strtotime($data['birthday']));
			}
			return $data;
		}
		
		protected function __before_save($data) {
			if(isset($data['pass'])) {
				$data['pass'] = hash('md5', hash('md5', $this->salt_table.$data['pass']).$this->salt_table);
			}
			if(isset($data['phone'])) {
				$data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
				if(strlen($data['phone']) > 10) {
					$data['phone'] = substr($data['phone'], -10);
				}
			}
			if(isset($data['birthday'])) {
				$data['birthday'] = date('Y-m-d 00:00:00', strtotime($data['birthday']));
			}
			return $data;
		}
		
		/**
		 * Проверка куки пользователя
		 */
		public function cookie_check() {
			$pattern = $this->_conf->get('admin/pattern_cookie');
			if(empty($_COOKIE)) {
			return false;
			}
			foreach($_COOKIE as $name => $value) {
				if(!preg_match($pattern, $name, $matches)) {
					continue;
				}
				$admin = $this->get_item($matches[1]);
				if(empty($admin['is_active']) || !empty($admin['is_delete'])) {
					continue;
				}
				if($value !== hash('md5', hash('md5', $admin['id'].$this->salt_cookie.$admin['pass']).$this->salt_cookie)) {
					continue;
				}
				
				$this->save(array(
					'id'				=> $admin['id'],
					'last_check_time'	=> date('Y-m-d H:i:s')
				));
				return $admin;
			}
			return false;
		}
		
		public function cookie_set($id, $pass, $expired = 0) {
			$value = hash('md5', hash('md5', $id.$this->salt_cookie.$pass).$this->salt_cookie);
			$this->_cookie->set('u'.$id, $value, $expired);
		}
		
		public function get_pass($id) {
			return $this->_sql->get_one('SELECT pass FROM '.$this->get_table().' WHERE id = '.(int)$id);
		}
		
		public function generate_pass() {
			$symbols = '23456789qazxswedvfrtgbnhyujmkp';
			
			$pass = '';
			for($i = 0; $i < 6; $i++) {
				$pass .= $symbols[rand(0, strlen($symbols) - 1)];
			}
			return $pass;
		}
		
		public function get_main() {
			$id = (int)$this->_conf->get('settings/main_admin_id');
			return $this->get_item($id);
		}
		
		public function hasRight($admin, $group_id) {
			if(empty($admin['group_id']) || (int)$admin['group_id'] !== (int)$group_id) {
				return false;
			}
			return true;
		}		
	
	}
?>