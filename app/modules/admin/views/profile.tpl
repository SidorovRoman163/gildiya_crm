<h1>Профиль</h1>
<form action="/admin/profile_save/" class="stdForm" method="post">
	<input type="hidden" name="id" value="{$id}" />
	<fieldset>
		<label for="fioField" class="stdFnt">Фамилия, имя:</label>
		<input class="stdTxt" type="text" name="fio" id="fioField" value="{$fio}" />
	</fieldset>
	<fieldset>
		<div class="pairLeft">
			<label for="passField" class="stdFnt">Пароль:</label>
			<input class="stdTxt" type="password" name="pass" id="passField" value="" />
		</div>
		<div class="pairRight">
			<label for="pass2Field" class="stdFnt">Подтвержение пароля:</label>
			<input class="stdTxt" type="password" name="pass2" id="pass2Field" value="" />
		</div>
	</fieldset>
	<fieldset>
		<div class="pairLeft">
			<label for="phoneField" class="stdFnt half">Телефон:</label>
			<input class="stdTxt" type="text" name="phone" id="phoneField" value="{$phone}" />
		</div>
		<div class="pairRight">
			<label for="emailField" class="stdFnt half">E-mail</label>
			<input class="stdTxt" type="text" name="email" id="emailField" value="{$email}" disabled="disabled" />
		</div>
		<div class="clear">&nbsp;</div>
	</fieldset>
	<fieldset class="buttonsBar">
		<input type="submit" class="stdButton saveButton" value="" />
	</fieldset>
</form>