{if !empty($id)}
	<div id="info" class="radius">
		<p>
			<span class="item"><b>Время создания:</b></span>
			<span class="item">{$insert_time_format}</span>
		</p>
		<p>
			<span class="item"><b>Статус приглашения:</b></span>
			<span class="item">{if !empty($is_invate)}Приглашен{else}Не приглашен{/if}</span>
		</p>
	</div>
{/if}