<h1>{$header}</h1>
<form action="/admin/save/" class="stdForm validateForm" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id" value="{$id}" />
	<fieldset>
		<label for="emailField" class="stdFnt">E-mail</label>
		<input class="stdTxt vfNotEmpty" type="text" name="email" id="emailField" value="{$email}" placeholder="first@domain.ru, second@domain.ru, ..." />
	</fieldset>
	{if !empty($id) && !empty($is_invate)}
		<fieldset>
			<label for="passField" class="stdFnt">Пароль</label>
			<input class="stdTxt" type="password" name="pass" id="passField" value="" />
		</fieldset>
	{/if}
	<fieldset>
		<div class="pairLeft half">
			<label for="fioField" class="stdFnt">Фамилия, имя</label>
			<input class="stdTxt vfNotEmpty" type="text" name="fio" id="fioField" value="{$fio}" />
		</div>
		<div class="pairRight half">
			<label for="phoneField" class="stdFnt half">Телефон</label>
			<input class="stdTxt" type="text" name="phone" id="phoneField" value="{$phone}" />
		</div>
	</fieldset>
	<fieldset>
		<label for="groupField" class="stdFnt">Группа</label>
		<select name="group_id" id="groupField" class="styled">
			{foreach $admin_group_list as $group}
				<option value="{$group.id}" {if $group.selected}selected="selected"{/if}>{$group.title}</option>
			{/foreach}
		</select>
	</fieldset>
	<!--
	<fieldset>
		<label for="stateField" class="stdFnt">Статус</label>
		<select name="is_active" id="stateField2" class="styled">
			<option value="0">Скрыт</option>
			<option value="1"{if !empty($is_active)} selected="selected"{/if}>Активен</option>
		</select>
	</fieldset>
	-->
	<fieldset class="buttonsBar">
		<input type="submit" class="stdButton saveButton" value="" />
		{if !empty($id)}<input type="button" class="stdButton deleteButton" rel="/admin/delete/{$id}/" value="" />{/if}
		{if !empty($id) && empty($is_invate)}<input type="button" class="stdButton invateButton" onClick="location.href='/admin/invate/{$id}/';" value="" />{/if}
	</fieldset>
</form>