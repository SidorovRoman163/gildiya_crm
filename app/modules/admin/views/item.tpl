<h1>{$fio}</h1>
<div id="user_info">
	<table>
		{if $email}
			<tr><td class="label">E-mail:</td>
				<td>{$email}</td></tr>
		{/if}
		{if $phone}
			<tr><td class="label">Телефон:</td>
				<td>{$phone}</td></tr>
		{/if}
		{if $group_title}
			<tr><td class="label">Группа:</td>
				<td>{$group_title}</td></tr>
		{/if}
		<tr><td></td>
			<td><a href="/admin/edit/{$id}/">Редактировать</a></td></tr>
	</table>
</div>