<form id="filterWrapper" class="filterWrapperRight">
	<input type="hidden" name="page" value="{$page}" id="filterPage" />
	<input type="hidden" name="sort" value="{$sort}" id="filterSort" />
	<select id="filterGroup" class="styled filterSelStd" name="group_id" style="width:200px;">
		<option value="0">Все группы</option>
		{foreach $group_list as $group}
			<option value="{$group.id}" {$group.selected}>{$group.title} ({$group.count})</option>
		{/foreach}
	</select>
</form>
<h1>Пользователи</h1>
<div class="buttonBar">
	<a href="/admin/add/" class="button add">Добавить пользователя</a>
</div>
{if $admins_list}
	<table class="stdTbl">
		<tr>
			<th></th>
			<!-- <th></th> -->
			<th class="{if $sort == 'fio:asc'}asc{else}{if $sort == 'fio:desc'}desc{/if}{/if}">
				<a href="javascript:void(0);" rel="fio:{if $sort == 'fio:asc'}desc{else}asc{/if}">Фамилия, имя</a></th>
			<th class="{if $sort == 'email:asc'}asc{else}{if $sort == 'email:desc'}desc{/if}{/if}">
				<a href="javascript:void(0);" rel="email:{if $sort == 'email:asc'}desc{else}asc{/if}">E-mail</a></th>
			<th class="{if $sort == 'phone:asc'}asc{else}{if $sort == 'phone:desc'}desc{/if}{/if}">
				<a href="javascript:void(0);" rel="phone:{if $sort == 'phone:asc'}desc{else}asc{/if}">Телефон</a></th>
			<th class="{if $sort == 'group_id:asc'}asc{else}{if $sort == 'group_id:desc'}desc{/if}{/if}">
				<a href="javascript:void(0);" rel="group_id:{if $sort == 'group_id:asc'}desc{else}asc{/if}">Группа</a></th>
			{if $group_id == 2}
				<th><nobr>Компания<nobr></th>
			{/if}
		</tr>
		{foreach $admins_list as $admin}
			<tr class="{if $admin.even()}even{else}odd{/if}">
				<!-- <td class="{if !empty($admin.is_active)}isActive{else}isNotActive{/if}">
					<span title="{if !empty($admin.is_active)}Активен{else}Скрыт{/if}"></span>
				</td> -->
				<td class="delButton"><a href="/admin/delete/{$admin.id}/" title="Удалить"></a></td>
				<td>
					<a href="/admin/item/{$admin.id}/">{$admin.fio}</a>
					{if $admin.post}
						<br /><span style="color:gray;">{$admin.post}</span>
					{/if}
				</td>
				<td width="1%"><nobr><a href="mailto:{$admin.email}">{$admin.email}</a></nobr></td>
				<td width="1%"><nobr>{$admin.phone}</nobr></td>
				<td width="1%"><nobr>{$admin.admin_group_title}</nobr></td>
				{if $group_id == 2}
					<td width="1%"><nobr><a href="/dealers/item/{$admin.dealer_id}/">{$admin.dealer_title}</a></nobr></td>
				{/if}
			</tr>
		{/foreach}
	</table>
	{if $pages_list}
		<div class="pagination">
			<a href="javascript:void(0);" rel="{$prev_page}" class="prev {$prev_class}">{$lang_prev}</a>
			{foreach $pages_list as $p}
				<a href="javascript:void(0);" rel="{$p.page}" class="{$p.active}">{$p.num}</a>
			{/foreach}
			<a href="javascript:void(0);" rel="{$next_page}" class="next {$next_class}">{$lang_next}</a>
		</div>
	{/if}
{/if}