(function($) {

	$('#addLinkPosTable').live('click', function() {
		$('#posTableBody').append('<tr class="posTableTdToDel">'+$('#posTableBody tr').html()+'</tr>');
		$('#posTableBody tr:last input, #posTableBody tr:last textarea').val('');
	});
	
	$('#posTableBody .posTableDel a').live('click', function() {
		$(this).parents('tr').remove();
		return false;
	});
	
})(jQuery)