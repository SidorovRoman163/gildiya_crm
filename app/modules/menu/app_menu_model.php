<?php
	class menu_model extends core_model {
		
		protected function __init_model() {
			
			// определяем структуру данных модели
			$this->struct = array(
				'title'	=> array(
					'type'		=> self::T_STRING,
					'length'	=> 100,
					'valid'		=> self::V_NOT_EMPTY,
				),
				'url'	=> array(
					'type'		=> self::T_STRING,
					'length'	=> 100,
					'valid'		=> self::V_NOT_EMPTY,
				),
				'sort'	=> array(
					'type'		=> self::T_INT,
					'valid'		=> self::V_NOT_EMPTY,
				),
			);
			
		}
		
		// public function __after_get($data) {
			// return $data;
		// }
		
		public function admin_group_map($admin_groups = array(), $menu_id = 0) {
			if(empty($admin_groups)) {
				return false;
			}
			
			$sql =  'SELECT 1 FROM `'.$this->_conf->get('db/pref').'_admin_group_menu_map` '.
					'WHERE '.
						'`admin_group_id` IN('.join(',', $admin_groups).') AND '.
						'`menu_id` = '.(int)$menu_id;
			return (bool)$this->_sql->get_one($sql);
		}
		
	}
?>