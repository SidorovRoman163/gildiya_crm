<?php
	class menu_controller extends app_controller {
		
		public function generate() {
			
			// получаем 1-ый уровень
			$sql =  'SELECT m.id, m.title, m.url '.
					'FROM '.self::$pref.'_menu m '.
						'LEFT JOIN '.self::$pref.'_admin_group_menu_map agmm ON m.id = agmm.menu_id '.
					'WHERE m.is_delete = 0 AND m.pid = 0 AND agmm.admin_group_id = '.(int)self::$admin['group_id'].' '.
					'GROUP BY m.id '.
					'ORDER BY m.sort, m.title, m.id';
			$data['menu'] = $this->_sql->get_all($sql);
			if(!empty($data['menu'])) {
				foreach($data['menu'] as $i => &$item) {
					
					$item['classes'] = array();

					if(self::$volume_id == $item['id']) {
						$item['classes'][] = 'active';
					}
					
					// получаем циферки в пунктах меню
					switch($item['id']) {
						case 19: // Лента событий
							$item['count'] = $this->events->get_unreadable_count(self::$admin['id']);
							break;
					}
					
					// получаем 2-ой уровень
					$sql =  'SELECT m.id, m.title, m.url '.
							'FROM '.self::$pref.'_menu m '.
								'LEFT JOIN '.self::$pref.'_admin_group_menu_map agmm ON m.id = agmm.menu_id '.
							'WHERE m.is_delete = 0 AND m.pid = '.(int)$item['id'].' AND agmm.admin_group_id = '.(int)self::$admin['group_id'].' '.
							'GROUP BY m.id '.
							'ORDER BY m.sort';
					$item['submenu'] = $this->_sql->get_all($sql);
					if(empty($item['submenu'])) {
						$item['submenu'] = false;
						$item['classes'] = join(' ', $item['classes']);
						continue;
					}
					
					foreach($item['submenu'] as $j => &$subitem) {

//						// получаем циферки в пунктах меню
//						switch($subitem['id']) {
//							case 15: // Дилеры
//								$subitem['count'] = $this->dealers->getAllCount();
//								break;
//							case 16: // Контакты
//								$subitem['count'] = $this->dealers->getAllPersonsCount();
//								break;
//						}

						// формируем классы подменю
						$subitem['classes'] = array();
						if(self::$volume_id == $subitem['id']) {
							$item['classes'][] = 'preactive';
							$subitem['classes'][] = 'active';
						}
						$subitem['classes'] = join(' ', $subitem['classes']);
					}
					
					$item['classes'] = join(' ', $item['classes']);
				}
				
				$this->_js->assign('menu_block');
				$this->_css->assign('menu_block');
				$this->_tpl->render('menu/menu_block', $data, 'menu_block');
			}
		}
		
	}
?>