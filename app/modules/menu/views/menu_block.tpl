<ul id="menu_block">
	{foreach $menu as $item}
		<li class="{$item.classes}">
			<a id="menu_link_{$item.id}" href="{if !empty($item.url)}{$item.url}{else}javascript:void(0);{/if}">
				{$item.title}
				{if $item.count}
					<span id="menu_count_{$item.id}" class="count radius">{$item.count}</span>
				{/if}
			</a>
			{if $item.submenu}
				<ul class="submenu">
					{foreach $item.submenu as $subitem}
						<li class="{$subitem.classes}" data-id="{$subitem.id}">
							<a href="{if !empty($subitem.url)}{$subitem.url}{else}javascript:void(0);{/if}">
								{$subitem.title}
								{if $subitem.count}
									<span class="count radius">{$subitem.count}</span>
								{/if}
							</a>
						</li>
					{/foreach}
				</ul>
			{/if}
		</li>
	{/foreach}
</ul>

