(function($) {

	// отмечаем событие как прочитанное
	$(document).on('change', '#events_list_table .checkbox', function() {
		if(this.checked) {
			$(this).attr('disabled', 'disabled');
			var row = $(this).parents('tr');
			
			$.ajax({
				url: '/main/check_event/',
				data: {event_id: $(this).attr('rel')},
				type: 'post',
				cache: false,
				dataType: 'json',
				success: function(r) {
					switch(r.state) {
						case 'ok':
							row.addClass('gray');
							setTimeout(function() {
								row.remove();
								$('#events_list_table tr').removeClass('odd').removeClass('even');
								$('#events_list_table tr:even').addClass('even');
								$('#events_list_table tr:odd').addClass('odd');
							}, 200);
							
							// количество не прочитанных
							if(r.count > 0) {
								$('#menu_count_19').text(r.count);
							} else {
								$('#menu_count_19').remove();
							}
							break;
						case 'error':
							break;
					}
				}
			});
		}
	});

})(jQuery);