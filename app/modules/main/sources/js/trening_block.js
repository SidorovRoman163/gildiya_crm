(function($) {

	$(document).ready(function() {
		
		/**
		 * Кнопка "Зарегистрироваться на треннинг"
		 */
		$('#trening_block .button').click(function() {
			$.getJSON('/main/trening_popup/', function(r) {
				if(r.status == 'ok') {
					$('#trening_popup').remove();
					$('#trening_shadow').remove();
					$('body').append(r.content);
					
					/**
					 * Отправка формы
					 */
					$('#trening_popup_form').data('onValid', function() {
						var form	= $('#trening_popup_form');
						var submit	= form.find('.button:first');
						
						if(submit.hasClass('disabled')) {
							return false;
						}
						submit.addClass('disabled');
						
						$.ajax({
							url: form.attr('action'),
							type: form.attr('method'),
							data: form.serialize(),
							dataType: 'json',
							success: function(r) {
								if(r.status == 'ok') {
									$('#trening_popup').fadeOut(function() {
										$('#trening_popup').remove();
										$('#trening_shadow').remove();
									});
									alert('Регистрация на тренинг LADA Sport успешно завершена!');
								} else {
									submit.removeClass('disabled');
									if(r.status == 'error' && r.type) {
										switch(r.type) {
											case 'empty':
												switch(r.field) {
													case 'family_name':	alert('Фамилия не указана'); break;
													case 'first_name':	alert('Имя не указано'); break;
													case 'patronymic':	alert('Отчество не указано'); break;
													case 'post':		alert('Должность не указана'); break;
													case 'phone':		alert('Телефон не указан'); break;
													case 'email':		alert('E-mail не указан'); break;
												}
												break;
											case 'incorrect':
												switch(r.field) {
													case 'phone':		alert('Телефон указан некорректно'); break;
													case 'email':		alert('E-mail указан некорректно'); break;
												}
												break;
											case 'not_sended':
												alert('Ошибка при отправке почтового сообшения.\nПопробуйте повтроить запрос позже.');
												break;
										}
									}
								}
							}
						});
						
						return false;
					});
				} else {
					alert('Регистрация на тренинг завершена');
				}
			});
			return false;
		});
		
		$(document).on('click', '#trening_shadow, #trening_close', function() {
			$('#trening_popup').fadeOut(function() {
				$('#trening_popup').hide();
				$('#trening_shadow').hide();
			});
		});
		
		$(document).on('click', '#trening_close', function() {
			$('#trening_shadow').trigger('click');
		});
		
	});
	
})(jQuery);