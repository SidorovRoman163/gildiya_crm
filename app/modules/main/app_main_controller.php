<?php
	class main_controller extends app_controller {
		
		public function __init() {
			self::$volume_id = 19;
		}
		
		public function index() {
			
			// получаем тренинг
			$data['trening_block'] = $this->trening_block();

			// получаем список событий
			$sql  = 'SELECT em.*, e.title, fr.fio user_from_fio '.
					'FROM '.self::$pref.'_events_map em '.
						'LEFT JOIN '.self::$pref.'_events e	ON em.event_id = e.id '.
						'LEFT JOIN '.self::$pref.'_admin fr	ON em.user_from_id = fr.id '.
					'WHERE em.user_to_id = '.(int)self::$admin['id'].' AND em.is_checked = 0 '.
					'ORDER BY em.insert_time DESC '.
					'LIMIT 50';
			$data['events_list'] = $this->_sql->get_all($sql);
			if(!empty($data['events_list'])) {
				foreach($data['events_list'] as $i => &$item) {
					
					// аватар создателя события
					if($this->_file->image_exists('admin/avatars/s32', $item['user_from_id'])) {
						$item['user_from_avatar'] = $this->_file->image_tag(32, 32);
					}
					
					$item['time'] = strtotime($item['insert_time']);
					$item['time'] = $this->_date->common($item['time']).'<br />в '.date('H:i:s', $item['time']);
					
					switch((int)$item['event_id']) {
						case 1:	// новая задача
						case 2: // исполнитель принимает задачу
						case 3: // исполнитель условно завершает задачу
						case 4: // постановщик возобновляет задачу
						case 5: // исполнитель возобновляет задачу
						case 6: // постановщик закрывает задачу
						case 7: // задача была отредактирована
						case 8: // задача была удалена
							$sql  = 'SELECT t.id, t.title, t.project_id, p.title project_title, '.
										't.owner_id, own.fio owner_fio, '.
										't.responsible_id, res.fio responsible_fio '.
									'FROM '.self::$pref.'_tasks t '.
										'LEFT JOIN '.self::$pref.'_admin own	ON t.owner_id		= own.id '.
										'LEFT JOIN '.self::$pref.'_admin res	ON t.responsible_id = res.id '.
										'LEFT JOIN '.self::$pref.'_projects p	ON t.project_id		= p.id '.
									'WHERE t.id = '.(int)$item['subject_id'];
							$task = $this->_sql->get_row($sql);
							if(empty($task)) {
								unset($data['events_list'][$i]);
								continue;
							}
							
							switch((int)$item['event_id']) {
								case 1: // новая задача
									$item['text'] =
										'<a href="/tasks/item/'.$task['id'].'/">'.$task['title'].'</a>'.
										(!empty($task['project_id']) ? '<br />'.$task['project_title'] : '');
									break;
								case 2: // исполнитель принимает задачу
									$item['text'] =
										'<a href="/admin/item/'.$task['responsible_id'].'/">'.$task['responsible_fio'].'</a> '.
										'приступил к выполнению вашей задачи: '.
										'&laquo;<a href="/tasks/item/'.$task['id'].'/">'.$task['title'].'</a>&raquo;'.
										(!empty($task['project_id']) ? ' в проекте '.$task['project_title'] : '');
									break;
								case 3: // исполнитель условно завершает задачу
									$item['text'] =
										'<a href="/admin/item/'.$task['responsible_id'].'/">'.$task['responsible_fio'].'</a> '.
										'условно завершил выполнение вашей задачи: '.
										'&laquo;<a href="/tasks/item/'.$task['id'].'/">'.$task['title'].'</a>&raquo;'.
										(!empty($task['project_id']) ? ' в проекте '.$task['project_title'] : '');
									break;
								case 4: // постановщик возобновляет задачу
									$item['text'] =
										'<a href="/admin/item/'.$task['owner_id'].'/">'.$task['owner_fio'].'</a> '.
										'возобновил выполнение ранее завершенной вами задачи: '.
										'&laquo;<a href="/tasks/item/'.$task['id'].'/">'.$task['title'].'</a>&raquo;'.
										(!empty($task['project_id']) ? ' в проекте '.$task['project_title'] : '');
									break;
								case 5: // исполнитель возобновляет задачу
									$item['text'] =
										'<a href="/admin/item/'.$task['responsible_id'].'/">'.$task['responsible_fio'].'</a> '.
										'возобновил выполнение ранее завершенной вашей задачи: '.
										'&laquo;<a href="/tasks/item/'.$task['id'].'/">'.$task['title'].'</a>&raquo;'.
										(!empty($task['project_id']) ? ' в проекте '.$task['project_title'] : '');
									break;
								case 6: // постановщик закрывает задачу
									$item['text'] =
										'<a href="/admin/item/'.$task['owner_id'].'/">'.$task['owner_fio'].'</a> '.
										'завершил задачу: '.
										'&laquo;<a href="/tasks/item/'.$task['id'].'/">'.$task['title'].'</a>&raquo;'.
										(!empty($task['project_id']) ? ' в проекте '.$task['project_title'] : '');
									break;
								case 7: // задача была отредактирована
									$item['text'] =
										'<a href="/admin/item/'.$item['user_from_id'].'/">'.$item['user_from_fio'].'</a> '.
										'отредактировал задачу: '.
										'&laquo;<a href="/tasks/item/'.$task['id'].'/">'.$task['title'].'</a>&raquo;'.
										(!empty($task['project_id']) ? ' в проекте '.$task['project_title'] : '');
									break;
								case 8: // задача была удалена
									$item['text'] =
										'<a href="/admin/item/'.$item['user_from_id'].'/">'.$item['user_from_fio'].'</a> '.
										'удалил задачу: '.
										'&laquo;<a href="/tasks/item/'.$task['id'].'/">'.$task['title'].'</a>&raquo;'.
										(!empty($task['project_id']) ? ' в проекте '.$task['project_title'] : '');
									break;
							}
							break;
						case  9: // подзадача создана
						case 10: // подзадача отредактирована
						case 11: // подзадача удалена
						case 12: // подзадача выполнена
						case 13: // подзадача возобновлена
							$sql  = 'SELECT tc.id, tc.title, tc.task_id, tc.owner_id, own.fio owner_fio, '.
										't.owner_id task_owner_id, t.responsible_id responsible_owner_id, '.
										't.title task_title, t.project_id, p.title project_title '.
									'FROM '.self::$pref.'_tasks_checklist tc '.
										'LEFT JOIN '.self::$pref.'_tasks t		ON tc.task_id		= t.id '.
										'LEFT JOIN '.self::$pref.'_admin own	ON tc.owner_id		= own.id '.
										'LEFT JOIN '.self::$pref.'_admin town	ON t.owner_id		= town.id '.
										'LEFT JOIN '.self::$pref.'_admin tres	ON t.responsible_id = tres.id '.
										'LEFT JOIN '.self::$pref.'_projects p	ON t.project_id		= p.id '.
									'WHERE tc.id = '.(int)$item['subject_id'];
							$check = $this->_sql->get_row($sql);
							if(empty($check)) {
								unset($data['events_list'][$i]);
								continue;
							}
							
							switch((int)$item['event_id']) {
								case 9: // подзадача создана
									$sql  = 'SELECT value FROM '.self::$pref.'_events_map_params '.
											'WHERE event_map_id = '.(int)$item['id'].' AND name="text"';
									$check['text'] = $this->_sql->get_one($sql);
									
									$item['text'] =
										'<a href="/admin/item/'.$check['owner_id'].'/">'.$check['owner_fio'].'</a> '.
										'создал подзадачу &laquo;'.$check['text'].'&raquo; в задаче: '.
										'&laquo;<a href="/tasks/item/'.$check['task_id'].'/">'.$check['task_title'].'</a>&raquo;'.
										(!empty($check['project_id']) ? ' в проекте '.$check['project_title'] : '');
									break;
								case 10: // подзадача отредактирована
									$sql  = 'SELECT value FROM '.self::$pref.'_events_map_params '.
											'WHERE event_map_id = '.(int)$item['id'].' AND name="text_old"';
									$check['text_old'] = $this->_sql->get_one($sql);
									
									$sql  = 'SELECT value FROM '.self::$pref.'_events_map_params '.
											'WHERE event_map_id = '.(int)$item['id'].' AND name="text_new"';
									$check['text_new'] = $this->_sql->get_one($sql);
									
									$item['text'] =
										'<a href="/admin/item/'.$check['owner_id'].'/">'.$check['owner_fio'].'</a> '.
										'отредактировал подзадачу с &laquo;'.$check['text_old'].'&raquo; на &laquo;'.$check['text_new'].'&raquo; в задаче: '.
										'&laquo;<a href="/tasks/item/'.$check['task_id'].'/">'.$check['task_title'].'</a>&raquo;'.
										(!empty($check['project_id']) ? ' в проекте '.$check['project_title'] : '');
									break;
							}
							
							break;
					}
				}
				$data['events_list'] = array_values($data['events_list']);
			}
			
			$this->_tpl->render('main/index', $data, 'content');
		}
		
		public function check_event() {
			
			// проверка на наличие event_id
			if(empty(self::$post['event_id'])) {
				die(json_encode(array(
					'state'		=> 'error',
					'message'	=> 'empty_event_id'
				)));
			}
			
			// получаем событие и проверяем его принадлежность пользователю
			$sql  = 'SELECT em.id '.
					'FROM '.self::$pref.'_events_map em '.
					'WHERE '.
						'em.id			= '.(int)self::$post['event_id'].' AND '.
						'em.user_to_id	= '.(int)self::$admin['id'].' AND '.
						'em.is_checked	= 0';
			$event_id = $this->_sql->get_one($sql);
			if(empty($event_id)) {
				die(json_encode(array(
					'state'		=> 'error',
					'message'	=> 'permission_denied'
				)));
			}
			
			// отмечаем событие как прочитанное
			$this->_sql->update(self::$pref.'_events_map', array(
				'is_checked'	=> '1',
				'checked_time'	=> date('Y-m-d H:i:s')
			), $event_id);
			
			// говорим, что все прекрасно
			die(json_encode(array(
				'state'	=> 'ok',
				'count' => (int)$this->events->get_unreadable_count(self::$admin['id'])
			)));
		}
		
		/**
		 * Тренинг-блок
		 */
		private function trening_block() {
			$data = array();
		
			$this->_js->assign('main/trening_block');
			$this->_css->assign('main/trening_block');
			
			return $this->_tpl->render('main/trening_block', $data);
		}
		
		/**
		 * Тренинг-блок - всплывающее окно
		 */
		public function trening_popup() {
//			die(json_encode(array(
//				'status'	=> 'error'
//			)));
			
			die(json_encode(array(
				'status'	=> 'ok',
				'content'	=> $this->_tpl->render('main/trening_popup', array())
			)));
		}
		
		/**
		 * Тренинг-блок - отправка формы
		 */
		public function trening_popup_send() {
			
			// первичная валидация
			foreach(array('family_name', 'first_name', 'patronymic', 'post', 'phone', 'email') as $field) {
				if(empty(self::$post[$field])) {
					die(json_encode(array(
						'status'	=> 'error',
						'type'		=> 'empty',
						'field'		=> $field
					)));
				}
			}
			
			// проверка телефона
			$phone = $this->_format->phone(self::$post['phone']);
			if(empty($phone)) {
				die(json_encode(array(
					'status'	=> 'error',
					'type'		=> 'incorrect',
					'field'		=> 'phone'
				)));
			}
			self::$post['phone'] = $phone;
			
			// проверка E-mail
			if(!filter_var(self::$post['email'], FILTER_VALIDATE_EMAIL)) {
				die(json_encode(array(
					'status'	=> 'error',
					'type'		=> 'incorrect',
					'field'		=> 'email'
				)));
			}
			
			
			// проверка на принадлежность дилеру
			$sql  = 'SELECT d.id, d.title, d.phones, d.site '.
					'FROM '.self::$pref.'_dealers d '.
					'WHERE d.admin_id = '.(int)self::$admin['id'].' AND d.is_delete = 0';
			$dealer = $this->_sql->get_row($sql);
			
			// формируем данные
			$data = self::$post;
			if(!empty($dealer)) {
				$data['dealer_id']		= $dealer['id'];
				$data['dealer_title']	= $dealer['title'];
				$data['dealer_phones']	= $dealer['phones'];
				$data['dealer_site']	= $dealer['site'];
			}
			
			// формируем письмо администратору
			$letter	= $this->_tpl->render('settings/layout_blank', array(
				'text'	=> $this->_tpl->render('main/_letter_admin', $data) 
			));
			$subject	= 'Новая заявка на регистрацию на тренинг';
			$email		= 'pr@lada-sport.ru';
			
			// отправляем письмо
			if(!$this->_mail->send_mail($email, $letter, $subject)) {
				die(json_encode(array(
					'status'	=> 'error',
					'type'		=> 'not_sended'
				)));
			}
			
			// формируем письмо пользователю
			$letter	= $this->_tpl->render('settings/layout_blank', array(
				'text'	=> $this->_tpl->render('main/_letter_user', $data) 
			));
			$subject	= 'Регистрация на тренинг';
			$email		= self::$post['email'];
			
			// отправляем письмо
			if(!$this->_mail->send_mail($email, $letter, $subject)) {
				die(json_encode(array(
					'status'	=> 'error',
					'type'		=> 'not_sended'
				)));
			}
			
			// все ок
			die(json_encode(array(
				'status' => 'ok'
			)));
		}

	}
?>