<div id="trening_shadow"></div>
<div id="trening_popup">
	<a id="trening_close" href="#" title="Закрыть окно"></a>
	<h2>Регистрация на тренинг</h2>
	<form id="trening_popup_form" class="stdForm validateForm" method="post" action="/main/trening_popup_send/">
		<fieldset>
			<input type="text" name="family_name" placeholder="Фамилия" class="stdTxt vfNotEmpty" id="familyNameField" value="" />
		</fieldset>
		<fieldset>
			<input type="text" name="first_name" placeholder="Имя" class="stdTxt vfNotEmpty" id="firstNameField" value="" />
		</fieldset>
		<fieldset>
			<input type="text" name="patronymic" placeholder="Отчество" class="stdTxt vfNotEmpty" id="patronymicField" value="" />
		</fieldset>
		<fieldset>
			<input type="text" name="post" placeholder="Должность" class="stdTxt vfNotEmpty" id="postField" value="" />
		</fieldset>
		<fieldset>
			<input type="text" name="phone" placeholder="Контактный телефон" class="stdTxt vfNotEmpty" id="phoneField" value="" />
		</fieldset>
		<fieldset>
			<input type="text" name="email" placeholder="E-mail" class="stdTxt vfNotEmpty" id="emailField" value="" />
		</fieldset>
		<fieldset>
			<button class="button">Отправить</button>
		</fieldset>
	</form>
</div>