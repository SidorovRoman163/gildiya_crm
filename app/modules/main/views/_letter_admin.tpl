<p>Здравствуйте, администратор.<br />
<p>Поступила новая заявка на регистрацию на тренинг.</p>
<p><b>Данные отправителя</b></p>
<table border="0" cellpadding="5">
	<tr>
		<td align="right" valign="top"><b>Ф.И.О:</b></td>
		<td align="left"  valign="top">{$family_name} {$first_name} {$patronymic}</td>
	</tr>
	<tr>
		<td align="right" valign="top"><b>Должность:</b></td>
		<td align="left"  valign="top">{$post}</td>
	</tr>
	<tr>
		<td align="right" valign="top"><b>Контактный телефон:</b></td>
		<td align="left"  valign="top">{$phone}</td>
	</tr>
	<tr>
		<td align="right" valign="top"><b>E-mail:</b></td>
		<td align="left"  valign="top">{$email}</td>
	</tr>
</table>
{if !empty($dealer_id)}
	<p><b>Данные дилера</b></p>
	<table border="0" cellpadding="5">
		<tr>
			<td align="right" valign="top"><b>Компания:</b></td>
			<td align="left"  valign="top">{$dealer_title}</td>
		</tr>
		{if !empty($dealer_phones)}
			<tr>
				<td align="right" valign="top"><b>Телефоны:</b></td>
				<td align="left"  valign="top">{$dealer_phones}</td>
			</tr>
		{/if}
		{if !empty($dealer_site)}
			<tr>
				<td align="right" valign="top"><b>Сайт:</b></td>
				<td align="left"  valign="top"><a href="{$dealer_site}" target="_blank">{$dealer_site}</a></td>
			</tr>
		{/if}
		<tr>
			<td align="right" valign="top"><b>Страница в CRM:</b></td>
			<td align="left"  valign="top"><a href="http://crm.lada-sport.ru/dealers/item/{$dealer_id}/" target="_blank">http://crm.lada-sport.ru/dealers/item/{$dealer_id}/</a></td>
		</tr>
	</table>
{/if}