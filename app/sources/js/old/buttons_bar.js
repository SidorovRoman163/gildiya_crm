(function($) {

	$('#buttons_bar .actionButton').live('click', function() {
		
		if($('#buttons_bar').hasClass('loading')) {
			return false;
		}
		$('#buttons_bar').addClass('loading');
		
		var action = $(this).attr('rel');
		$.ajax({
			url: '/tasks/action/'+action+'/',
			type: 'post',
			data: {task_id: $('#buttons_bar').attr('rel')},
			dataType: 'json',
			success: function(r) {
				if(r.state == 'ok') {
					$('#buttons_bar').html(r.buttons_bar);
					
					if(r.state_title && $('#task_state_title').length > 0) {
						$('#task_state_title').text(r.state_title);
					}
				}
				$('#buttons_bar').removeClass('loading');
			}
		});
		
		return false;
	});
	
})(jQuery)