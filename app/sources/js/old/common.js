(function($){
	
	$(document).bind('dragstart', function(e) {
		e.preventDefault();
	});
	
	$('.jsSubmitButton').live('click', function() {
		var form	= $(this).parents('form');
		if(form.length) {
			form.find('#actionField').val($(this).attr('rel'));
			form.submit();
		}
	});
	
	$(document).ready(function() {
		
		cuSel({
			visRows: 10,
			changedEl: '.customSel',
			scrollArrows: true
		}); 		
		
		$('.castomTa').each(function() { $(this).find('.stdTa').css('width', (($(this).width()) - 1 + 'px')); }); 	 	
		$('.castomTa').change(function() { $(this).find('.stdTa').attr('name', $(this).find('.cuselActive').attr('val')); });
		
		$('.stdBtn, .stdChk, #help').mousedown(function() { if(!$(this).hasClass('disabled')) $(this).removeClass('hover').addClass('down'); });
		$('.stdBtn, .stdChk, #help').mouseup(function() { if(!$(this).hasClass('disabled')) $(this).removeClass('down').addClass('hover'); });
		$('.stdBtn, .stdChk, #help').hover(
			function() { $(this).removeClass('down').addClass('hover'); },
			function() { $(this).removeClass('down').removeClass('hover'); }
		);
		
		// && универсальный чекбокс (костя, не захерач его как в прошлый раз)
		$('.stdChk').live('click', function() {
			if(!$(this).hasClass('disabled')) {
				$('#' + $(this).attr('rel')).val(($(this).hasClass('active')) ? 0 : 1);  
				$(this).toggleClass('active');
			}
			return false;
		});
		
		// обработка кнопок в формах
		$('.stdButton')
			.live('mouseenter', function() {
				$(this).addClass('hoverButton');
			})
			.live('mouseleave', function() {
				$(this).removeClass('hoverButton');
			})
			.live('mousedown', function() {
				$(this).addClass('mousedownButton');
			});
		$(document).mouseup(function() {
			$('.mousedownButton').removeClass('mousedownButton');
		});
		
		// кнопка удаления "корзина" в таблице
		$('.delButton a').live('click', function() {
			return confirm('Вы уверены что хотите удалить?');
		});
		
		// кнопка удаления "Удалить" внутри формы
		$('.deleteButton').live('click', function() {
			if(confirm('Вы уверены что хотите удалить?')) {
				location.href = $(this).attr('rel');
			}
		});
		
		$('#profilePanelBtn').toggle(
			function() { $(this).parent().find('#profilePanel').show(); $(this).addClass('active'); },
			function() { $(this).parent().find('#profilePanel').hide(); $(this).removeClass('active'); }
		);
		
		$('.formTxt').focus(function() {
			//alert('!');
			if ($(this).val() == $(this).attr('rel')) {
				$(this).val(''); 
				$(this).addClass('stdFnt'); 
			}
		}).blur(function() { 
			if ($.trim($(this).val()) == '') {
				$(this).val($(this).attr('rel')); 
				$(this).removeClass('stdFnt');
			}
		});

		$('.stdTa').focus(function() { 
			if ($(this).html() == $(this).attr('rel')) {
				$(this).html(''); 
				$(this).addClass('stdFnt'); 
			}
		}).blur(function() {				
			if ($(this).html() == '') {
				$(this).html($(this).attr('rel')); 
				$(this).removeClass('stdFnt');
			}    
		});
		
	});	
	
	$('.checkboxListing a').live('click', function() {
		var obj = $('#'+$(this).attr('rel'));
		if(obj.length) {
			if($(this).hasClass('active')) {
				obj.val(0);
				$(this).removeClass('active');
			} else {
				obj.val(1);
				$(this).addClass('active');
			}
		}
	});
	
})(jQuery) 