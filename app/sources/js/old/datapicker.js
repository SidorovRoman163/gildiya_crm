(function($){
	
	$(document).ready(function() {	
		
		$.datepicker.setDefaults(
			$.extend($.datepicker.regional["ru"])
		);  
		  
		initDatapicker();
	});
	
	initDatapicker = function() {
	
		$('.datetimeTxt').datetimepicker({
			dateFormat: 'dd.mm.yy',
			hourMin: 9,
			hourMax: 18,
			stepMinute: 5
		});
		
		$('.dateTxt').datepicker({
			dateFormat: 'dd.mm.yy'
		});
	}
	
	rosinter_customDatapicker = function() {
		$('.ui-datepicker-calendar').find('tr').not(':first').each(function() {							    
			$(this).find('td:first').addClass('firstTd');
			$(this).find('td:last').addClass('lastTd');  				
		});
		$('.ui-datepicker-calendar tr:last').addClass('lastTr');    
	};	
	
})(jQuery)
