(function() {
	
	$(window).load(function() {
		
		$('.pagination a').live('click', function() {
			if(!$(this).hasClass('inactive') && !$(this).hasClass('active')) {
				$('#filterPage').val($(this).attr('rel'));
				filterApply();
			}
			return false;
		});
		
		$('#content .stdTbl th a').live('click', function() {
			$('#filterSort').val($(this).attr('rel'));
			filterApply();
			return false;
		});
		
		filterInit();
	});
	
	filterInit = function() {
		$('#filterWrapper .filterSelStd').change(function() {
			$('#filterPage').val(0);
			filterApply();
		});
	}
	
	filterApply = function() {
		if(window.filterBeforeAjax) {
			window.filterBeforeAjax();
		}
		$.ajax({
			url:  location.href,
			type: 'post',
			data: $('#filterWrapper').serialize(),
			success: function(data) {
				$('#content').html(data);
				cuSel({
					visRows: 10,
					changedEl: '#filterWrapper select',
					scrollArrows: true
				});
				filterInit();
				if(typeof initDatapicker == 'function') {
					initDatapicker();
				}
				if(window.filterAfterAjax) {
					window.filterAfterAjax();
				}
			}
		});
	}

})(jQuery)
