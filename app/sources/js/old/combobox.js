(function($) {

	$.widget('ui.combobox', {
		options: {
			url: '',
			format: function(item) {
				if(!item || !item.id || !item.title) {
					return;
				}
				return {
					id:		item.id,
					label:	item.title,
					value:	item.title
				};
			}
		},
		_create: function(param) {
			this.wrapper = $('<span>')
				.addClass('stdCombobox')
				.insertBefore(this.element);
			// this.element.hide();
			
			this._createAutocomplete();
			this._createShowAllButton();
		},
		_createAutocomplete: function() {
			var value = this.element.val();
			
			this.input = $('<input>')
				.appendTo(this.wrapper)
				.val(this.element.attr('label'))
				.addClass('stdTxt')
				.autocomplete({
					delay: 0,
					minLength: 0,
					source: $.proxy(this, '_source')
				});
			
			this.input.data('ui-autocomplete')._renderItem = function(ul, item) {
				 return $('<li>')
					.append('<a class="user_item">'+item.label+'</a>')
					.appendTo(ul);
				};
			
			this._on(this.input, {
				autocompleteselect: function(event, ui) {
					this.element.val(ui.item.id);
				},
				autocompletechange: '_removeIfInvalid'
			});
		},
		_createShowAllButton: function() {
			var input = this.input,
			wasOpen = false;
			$('<a>')
				.attr('tabIndex', -1)
				.appendTo(this.wrapper)
				.addClass('stdComboboxDownBtn')
				.mousedown(function() {
					wasOpen = input.autocomplete('widget').is(':visible');
				})
				.click(function() {
					input.focus();
					if(wasOpen) {
						return;
					}
					input.val('');
					input.autocomplete('search', '');
				});
		},
		_source: function(request, response) {
			var self	= this;
			var input	= this.input;
			
			$.ajax({
				url: this.options.url,
				dataType: 'json',
				data: {
					q: input.val()
				},
				success: function(data) {
					if(!data) return false;
					response($.map(data.list, function(item) {
						return self.options.format(item);
					}));
				}
			});
		},
		_removeIfInvalid: function(event, ui) {
			if(ui.item) return false;
			
			this.input.val('');
			this.element.val('');
		},
		_destroy: function() {
			this.wrapper.remove();
			this.element.show();
		}
	});
	
})(jQuery)