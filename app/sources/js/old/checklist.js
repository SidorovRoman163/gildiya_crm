(function($) {
	
	// многоцелевая функция:
	// - закрывает поле ввода
	// - не допускает открытия нескольких полей ввода
	// - возвращает либо ссылку "Добавить ...", либо чекбокс
	function checkListAppendAddLink() {
		if($('#checklist .checklistTxtField').length) {
			if($('#checklist .add_link a').length) {
				var text = $('#checklist .checklistTxtField:first').val();
				var item = $('#checklist .checklistTxtField:first').parent();
				
				item.html('<input type="checkbox" class="checkbox" /><p>'+text+'</p>');
			} else {
				var add_link_title = 'Добавить первую подзадачу';
				if($('#checklist .item').length > 1) {
					add_link_title = 'Добавить ещё одну подзадачу';
				}
				$('#checklist .add_link').html('<a class="add_link_a" href="javascript:void(0);">'+add_link_title+'</a>');
			}
		}
	}
	
	// клик по ссылке "Добавить ..."
	$('#checklist .add_link_a').live('click', function() {
		checkListAppendAddLink();
		
		var item = $(this).parent();
		item.append('<input class="stdTxt checklistTxtField" type="text" />');
		item.find('.checklistTxtField:first').focus();
		$(this).remove();
		
		return false;
	});
	
	// клик по тексту для его редактирования
	$('#checklist .item p').live('dblclick', function() {
		checkListAppendAddLink();
		
		var item			= $(this).parent();
		var checklist_id	= item.attr('rel');
		var text			= $(this).text();
		
		item.html('<input class="stdTxt checklistTxtField" type="text" /><a class="deleteItem" href="javascript:void(0);"></a>');
		
		var input = item.find('.checklistTxtField');
		if(input.length) {
			input.data('checklist_id', checklist_id);
			input.data('text', text);
			input.val(text);
			input.focus();
		}
		
		return false;
	});
	
	// удаление существующей подзадачи
	$('#checklist .deleteItem').live('click', function() {
		var item			= $(this).parent();
		var checklist_id	= item.attr('rel');
		var text			= item.find('.checklistTxtField').val();
		
		if(item.hasClass('deleting')) {
			return false;
		}
		
		if(text != '' && !confirm('Вы уверены?')) {
			return false;
		}
		
		item.addClass('deleting');
		
		$.ajax({
			url: '/tasks/checklist/delete/',
			type: 'post',
			data: {
				checklist_id: checklist_id
			},
			dataType: 'json',
			success: function(r) {
				if(r.state == 'ok') {
					item.remove();
					if($('#checklist .item').length == 1) {
						$('#checklist .add_link_a').text('Добавить первую подзадачу');
					}
				} else {
					item.removeClass('deleting');
				}
			}
		});
		
		return false;
	});
	
	// завершение или возобновление подзадачи
	$('#checklist .item .checkbox').live('click', function() {
		var input			= $(this);
		var item			= input.parent();
		var checklist_id	= item.attr('rel');
		
		// блокируем чекбокс
		input.attr('disabled', 'disabled');
		
		// предварительно визуализируем успех действия
		if(input[0].checked) {
			item.addClass('through');
		} else {
			item.removeClass('through');
		}
		
		$.ajax({
			url: '/tasks/checklist/checked/',
			type: 'post',
			data: {
				checklist_id:	checklist_id,
				is_checked:		input[0].checked ? 1 : 0
			},
			dataType: 'json',
			success: function(r) {
				if(r.state == 'ok') {
					// перепроверяем статус, обновляя состояние подзадачи
					if(r.is_checked) {
						item.addClass('through');
					} else {
						item.removeClass('through');
					}
				}
				input.removeAttr('disabled');
			}
		});
	});
	
	// завершение добавления подзадачи, либо её редактирования
	// по нажатию на клавишу <Enter>, после чего переход в режим нового добавления
	$('#checklist .checklistTxtField').live('keyup', function(e) {
		
		if(e.keyCode != 13) {
			return;
		}
		
		var input			= $(this);
		var item			= input.parent();
		var text			= input.val();
		var task_id			= $('#checklist').attr('rel');
		var checklist_id	= parseInt(input.data('checklist_id'));
		
		// режима редактирования подзадачи
		if(!isNaN(checklist_id) && checklist_id > 0) {
			var old_text = input.data('text');
			
			// если изменений не было, то возвращаем как было
			if(old_text == text) {
				item.html('<input type="checkbox" class="checkbox" /><p>'+text+'</p>');
				return false;
			}
		} else {
			checklist_id = 0;
		}
		
		if(text == '') {
			checkListAppendAddLink();
			return;
		}
		
		input.attr('disabled', 'disabled');
		input.addClass('loaderField');
		
		$.ajax({
			url: '/tasks/checklist/save/',
			type: 'post',
			data: {
				checklist_id:	checklist_id,
				task_id:		task_id,
				text:			text
			},
			dataType: 'json',
			success: function(r) {
				if(r.state == 'ok') {
					if(checklist_id > 0) {
						item.html('<input type="checkbox" class="checkbox" /><p>'+text+'</p>');
					} else {
						$('<div class="item" rel="'+r.checklist_id+'">'+
							'<input type="checkbox" class="checkbox" />'+
							'<p>'+text+'</p>'+
						  '</div>').insertBefore(item);
					}
				}
				input.removeAttr('disabled');
				input.removeClass('loaderField');
				input.focus();
				input.val('');
			}
		});
	});
	
	// клие на свободном поле, дабы скрыть поле вводв подзадачи
	$(document).click(function(e) {
		var checklist = $(e.target).parents('#checklist');
		if(!checklist.length) {
			checkListAppendAddLink();
		}
	});
	
})(jQuery) 