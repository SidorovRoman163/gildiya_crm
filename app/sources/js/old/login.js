(function($){
	
	$(document).ready(function() {
		
		/**
		 * Отправка формы авторизации
		 */
		$('#login_form').submit(function() {
			$('#login .warning').hide(); 
			$.ajax({
				url: $(this).attr('action'),
				type: 'post',
				dataType: 'json',  
				data: $(this).serialize(),
				success: function(r) {
					switch(r.status) {
						case 'ok':
							location.reload();
							break;    
						case 'error':
							switch(r.message) {
								case 'empty_email':	r.message = 'Не введен e-mail'; break;
								case 'empty_pass':	r.message = 'Не введен пароль'; break;
								case 'not_found':	r.message = 'Пара e-mail/пароль не совподает'; break;
							}
							$('#login .warning').html(r.message).fadeOut('slow').fadeIn(); 
							$('#login #passTxt').val('');
							break;
					}	   
				}
			});
			return false;
		});
		
		$('#rememberTxt').click(function() {
			$('#rememberChk').click();
		});

		$('#forgot').click(function() {			
			$('#forgotHdn').val(1);
			$('#enter').submit();
			return false;
		});
		
		$('#forgotBtn').click(function() {			
			$.ajax({
				type: 'POST',
				url: location.href,
				dataType: 'json',  
				data: ({
					'email' 	: $('#login #forgotTxt').val(), 
					'forgot'  	:'check'					
				}),
				success: function(status) {  $('#login .warning').html(status.notice).fadeOut('slow').fadeIn(); }					
			});
			return false;
		});
	
	});	
	
})(jQuery);