function errorOn(obj, text) {
	window.has_errors = true;
	
	if($(obj).hasClass('errorField')) {
		return false;
	}
	$(obj).addClass('errorField');
	
	$('<div class="errorHint" style="position:relative;height:34px;top:0;left:0;z-index:0;">'+
		'<div style="position:absolute;top:0;left:0;padding:10px 5px;color:red;white-space:nowrap;z-index:1000;">'+text+'</div>'+
	'</div>').insertAfter(obj);
}

function errorOff(obj) {
	$(obj).removeClass('errorField');
	var next = $(obj).next();
	if(next.length && next.hasClass('errorHint')) {
		next.remove();
	}
}

$('.validateForm').live('submit', function() {

	window.has_errors = false;
	
	// проверка на заполненность
	$(this).find('.vfNotEmpty').each(function() {
		if($(this).hasClass('cusel')) {
			var value = parseInt($(this).find('input:first').val());
			if(isNaN(value) || !value) {
				errorOn(this, 'Необходимо заполнить поле');
			} else {
				errorOff(this);
			}
		} else if($(this).attr('name')) {
			if(!$(this).val() || $(this).val() == '0') {
				errorOn(this, 'Необходимо заполнить поле');
			} else {
				errorOff(this);
			}
		}
	});
	
	// проверка на соответствие формату числа
	$(this).find('.vfIsNum').each(function() {
		if($(this).hasClass('errorField')) {
			return true;
		}
		var value = $(this).val();
		if(value == '') {
			return true;
		}
		value = parseInt(value);
		if(isNaN(value)) {
			console.log($(this).val());
			console.log(value);
			errorOn(this, 'Поле должно представлять из себя число');
		} else {
			errorOff(this);
		}
	});
	
	// проверка на соответствие формату даты
	$(this).find('.vfIsDate').each(function() {
		if($(this).hasClass('errorField')) {
			return true;
		}
		var value = $(this).val();
		if(value == '') {
			return true;
		}
		if(!/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/.test(value)) {
			errorOn(this, 'Поле должно представлять из себя дату в фотмате "DD.MM.YYYY"');
		} else {
			errorOff(this);
		}
	});
	
	$(this).find('.vfIsDatetime').each(function() {
		if($(this).hasClass('errorField')) {
			return true;
		}
		var value = $(this).val();
		if(value == '') {
			return true;
		}
		if(!/^[0-9]{2}\.[0-9]{2}\.[0-9]{4} [0-9]{2}\:[0-9]{2}$/.test(value)) {
			errorOn(this, 'Поле должно представлять из себя дату в фотмате "DD.MM.YYYY hh:mm"');
		} else {
			errorOff(this);
		}
	});
	
	if(window.has_errors) {
		console.log(window.has_errors);
		return false;
	}
	
	$(this).find('input[type=submit]').attr('disabled', 'disabled');
	return true;
});
