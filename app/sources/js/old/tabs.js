(function() {
	
	$(document).ready(function() {
		
		/**
		 * Инициализация табов
		 */
		$('.stdTabs.switch').each(function() {
			$(this).find('li').each(function() {
				if(!$(this).hasClass('active')) {
					$($(this).find('a').attr('href')).hide();
				}
			});
		});
		
		/**
		 * Обработка переключения контента в табах
		 */
		$('.stdTabs.switch li').click(function() {
			var list	= $(this).parent();
			var old_li	= list.find('.active');
			var new_li	= $(this);
			var old_a	= old_li.find('a');
			var new_a	= new_li.find('a');
			
			// меняем активность
			old_li.removeClass('active');
			new_li.addClass('active');
			
			// меняем контент
			$(old_a.attr('href')).fadeOut(function() {
				$(new_a.attr('href')).show();
			});
			
			return false;
		});
	
	});

})(jQuery);