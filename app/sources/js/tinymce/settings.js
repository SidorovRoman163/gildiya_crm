function initTinyMCE(obj) {
	var id			= $(obj).attr('id');
	var settings	= {
		theme:			'modern',
		skin:			'katrina',
		selector:		'.wysiwyg12',
		language:		'ru',
		width:			'100%',
		height:			'300',
		menubar:		false,
		statusbar:		false,
		content_css:	'/app/sources/css/tiny_content.css?_='+Math.random(),
		convert_urls:	false
	};

	if($(obj).hasClass('small')) {
		settings.height		= '200';
		settings.plugins	= 'link';
		settings.toolbar	= 'bold italic link';
	} else {
		var pattern			= /^([0-9]+):([0-9]+)$/;
		var data			= $(obj).attr('rel');
		var image 			= '';
		if(pattern.test(data)) {
			data				= pattern.exec(data);
			settings.image_list = '/admin/all/images_list/'+data[1]+'/'+data[2]+'/';
			image				= 'image';
		}
		settings.plugins	= 'advlist autolink link '+image+' lists charmap print preview table code save';
		
		settings.toolbar	= [
			'undo',
			'styleselect',
			'bold italic',
			'alignleft aligncenter alignright',
			'bullist numlist',
			'link '+image,
			'table',
			'removeformat code'
		];
		
		if($(obj).data('save')) {
			settings.toolbar.unshift('save');
			settings.save_enablewhendirty = false;
			settings.save_onsavecallback = $(obj).data('save');
		}
		
		settings.toolbar = settings.toolbar.join(' | ');
	}

	new tinymce.Editor(id, settings, tinymce.EditorManager).render();
}

$(document).ready(function() {
	
	$('.wysiwyg').each(function() {
		initTinyMCE(this);
	});
	
});