(function() {
	
	$(document).ready(function() {
	
		$('#menu_block li').hover(function() {
			$(this).addClass('hover');
		}, function() {
			$(this).removeClass('hover');
		});
		
		$('#menu_block > li > a').click(function() {
			var li = $(this).parent();
			var submenu = li.find('.submenu');
			if(!submenu.length) {
				return true;
			}
			
			if(li.hasClass('animated')) {
				return false;
			}
			li.addClass('animated');
			
			if(submenu.hasClass('opened')) {
				submenu.removeClass('opened').slideUp(function() {
					li.removeClass('animated');
				});
			} else {
				if(li.hasClass('preactive')) {
					submenu.removeClass('opened').slideUp(function() {
						li.removeClass('preactive');
						li.removeClass('animated');
					});
				} else {
					submenu.addClass('opened').slideDown(function() {
						li.removeClass('animated');
					});
				}
			}
			
			return false;
		});
	
	});
	
})(jQuery)