(function($){
	
	$(document).ready(function() {
		
		/**
		 * Отправка формы авторизации
		 */
		$('#login_form').submit(function() {
			$('#login .warning').hide();
			$.ajax({
				url: $(this).attr('action'),
				type: 'post',
				dataType: 'json',  
				data: $(this).serialize(),
				success: function(r) {
					switch(r.status) {
						case 'ok':
							location.reload();
							break;    
						case 'error':
							switch(r.message) {
								case 'empty_email':	r.message = 'Не введен e-mail'; break;
								case 'empty_pass':	r.message = 'Не введен пароль'; break;
								case 'not_found':	r.message = 'Пара e-mail/пароль не совпадает'; break;
							}
							$('#login_form .warning').html(r.message).fadeOut('slow').fadeIn(); 
							$('#login_form #passTxt').val('');
							break;
					}	   
				}
			});
			return false;
		});
		
		/**
		 * Отправка формы восстановления пароля
		 */
		$('#forgot_form').submit(function() {
			$('#login .warning').hide(); 
			$.ajax({
				url: $(this).attr('action'),
				type: 'post',
				dataType: 'json',  
				data: $(this).serialize(),
				success: function(r) {
					switch(r.status) {
						case 'ok':
							$('#forgot_form').html(
								'<img width="100" height="100" src="/app/sources/img/logo.png" id="logo_svg">'+
								'<div class="warning radius">На ваш e-mail отпралено письмо с инструкциями по восстановлению пароля</div>'
							);
							$('#forgot_form .warning').fadeIn(); 
							break;    
						case 'error':
							switch(r.message) {
								case 'empty_email':		r.message = 'Не введен e-mail'; break;
								case 'user_not_found':	r.message = 'Пользователь с таким e-mail не найден'; break;
								case 'send_mail_error':	r.message = 'При отправке почты произошла ошибка'; break;
							}
							$('#forgot_form .warning').html(r.message).fadeOut('slow').fadeIn(); 
							$('#forgot_form #passTxt').val('');
							break;
					}
				}
			});
			return false;
		});
		
		/**
		 * Отправка формы смены пароля
		 */
		$('#change_pass_form').submit(function() {
			$('#login .warning').hide(); 
			$.ajax({
				url: $(this).attr('action'),
				type: 'post',
				dataType: 'json', 
				data: $(this).serialize(),
				success: function(r) {
					switch(r.status) {
						case 'ok':
							$('#change_pass_form').html(
								'<img width="100" height="100" src="/app/sources/img/logo.png" id="logo_svg">'+
								'<div class="warning radius">Пароль был успешно изменён</div>'
							);
							$('#change_pass_form .warning').fadeIn(function() {
								setTimeout(function() {
									location.href = '/';
								}, 1000);
							});
							break;    
						case 'error':
							$('#change_pass_form .warning').html(r.message).fadeOut('slow').fadeIn(); 
							break;
					}
				}
			});
			return false;
		});
		
		$('#rememberTxt').click(function() {
			$('#rememberChk').click();
		});
	
		/**
		 * Переход к форме восстановления пароля
		 */
		$('#forgot').click(function() {			
			$('#login_form').fadeOut(function() {
				$('#forgot_form').fadeIn('fast');
			});
			return false;
		});
		
		/**
		 * Переход к форме авторизации
		 */
		$('#to_login').click(function() {			
			$('#forgot_form').fadeOut(function() {
				$('#login_form').fadeIn('fast');
			});
			return false;
		});
		
	});	
	
})(jQuery);