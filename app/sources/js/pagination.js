(function() {
	
	var keyupLastTime = false;
	
	$(window).load(function() {
		
		$(document).on('click', '.pagination a', function() {
			if(!$(this).hasClass('inactive') && !$(this).hasClass('active')) {
				$('#filterPage').val($(this).attr('rel'));
				filterApply();
			}
			return false;
		});
		
		$(document).on('click', '#content .stdTbl th a', function() {
			$('#filterSort').val($(this).attr('rel'));
			filterApply();
			return false;
		});
		
		filterInit();
	});
	
	filterInit = function() {
		$('#filterWrapper .filterSelStd').change(function() {
			$('#filterPage').val(0);
			filterApply();
		});
		
		// текстовый поиск
		$('#filterWrapper #filterSearch').keyup(function() {
			keyupLastTime = new Date().getTime();
			setTimeout(function() {
				if(new Date().getTime() - keyupLastTime > 300) {
					filterApply();
				}
			}, 300);
		});
	}
	
	filterApply = function() {
		if(window.filterBeforeAjax) {
			window.filterBeforeAjax();
		}
		$.ajax({
			url:  location.href,
			type: 'post',
			data: $('#filterWrapper').serialize(),
			dataType: $('#filterWrapper').data('ajaxDataType') ? $('#filterWrapper').data('ajaxDataType') : 'html',
			success: function(data) {
				if($('#filterWrapper').data('ajaxSuccess')) {
					$('#filterWrapper').data('ajaxSuccess')(data);
				} else {
					$('#content').html(data);
					$('#filterWrapper select').selectric();
					filterInit();
					if(typeof initDatapicker == 'function') {
						initDatapicker();
					}
				}
				if(window.filterAfterAjax) {
					window.filterAfterAjax();
				}
			}
		});
	}

})(jQuery)
