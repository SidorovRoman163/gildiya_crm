(function($) {
	
	$(document).bind('dragstart', function(e) {
		e.preventDefault();
	});
	
	$(document).ready(function() {
		
		// кнопка удаления "Удалить" внутри формы
		$('.deleteButton').click(function() {
			if(confirm('Вы уверены что хотите удалить?')) {
				location.href = $(this).attr('rel');
			}
		});
		
		// всплывашка профиля
		$('#profilePanelBtn').click(function() {
			if(!$(this).hasClass('active')) {
				$(this).parent().find('#profilePanel').show();
				$(this).addClass('active');
			} else {
				$(this).parent().find('#profilePanel').hide();
				$(this).removeClass('active');
			}
		});
		
		// кaстомные стилизованные селекты
		$('select.styled').selectric();
		
		
	});
	
	// кастомные чекбоксы
	$(document).on('click', '.stdChk', function() {
		if(!$(this).hasClass('disabled')) {
			$('#'+$(this).attr('rel')).val(($(this).hasClass('active')) ? 0 : 1);  
			$(this).toggleClass('active');
		}
		return false;
	});
	
	// кнопка удаления "корзина" в таблице
	$(document).on('click', '.delButton a', function() {
		return confirm('Вы уверены что хотите удалить?');
	});
	
	// группа кастомных чекбоксов
	$(document).on('click', '.checkboxListing a', function() {
		var obj = $('#'+$(this).attr('rel'));
		if(obj.length) {
			if($(this).hasClass('active')) {
				obj.val(0);
				$(this).removeClass('active');
			} else {
				obj.val(1);
				$(this).addClass('active');
			}
		}
	});
	
})(jQuery)