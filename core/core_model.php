<?php
	abstract class core_model extends core_object {
	
		// определяем допустимые типы данных
		const T_INT					= 1;
		const T_DATE				= 2;
		const T_STRING				= 3;
		const T_BOOL				= 4;
		const T_TEXT				= 5;
		const T_TEXTFILE			= 6; 
		const T_IMAGE				= 7;

		// определяем допустимые ключи
		const I_PRIMARY				= 1;		// первичный ключ MySQL
		const I_INDEX				= 2;		// обычный индекс MySQL
		const I_UNIQUE				= 4;		// уникальный индекс MySQL
		const I_FULLTEXT			= 8;		// полнотекстовый индекс MySQL
		
		// определяем допустимые правила валидации
		const V_NOT_EMPTY			= 1;		// не пусто
		const V_IS_EMAIL			= 2;		// являемся валидным e-mail адресом
		
		// правила выполнения API-методов
		const R_WITHOUT_BEFORE_GET	=  1;		// не иcпользовать callback-метод self::__before_get()
		const R_WITHOUT_AFTER_GET	=  2;		// не иcпользовать callback-метод self::__after_get()
		const R_WITHOUT_BEFORE_SAVE	=  4;		// не иcпользовать callback-метод self::__before_save()
		const R_WITHOUT_AFTER_SAVE	=  8;		// не иcпользовать callback-метод self::__before_save()
		const R_WITHOUT_RECURSIVE	= 16;		// не иcпользовать рекурсивную выборку отношений
		
		// определяем допустимые отношения
		const R_AGGREGATE			= 1;		// агрегативная связь (родитель владеет потомками)
		const R_ASSOCIATE			= 2;		// ассоциативная связь (потомки расширяют родителя)
		
		protected $name				= null;		// имя модели (имя класса)
		protected $has_insert_time	= true;		// имеет ли модель служебное поле "insert_time"?
		protected $has_update_time	= true;		// имеет ли модель служебное поле "update_time"?
		protected $has_delete_time	= true;		// имеет ли модель служебное поле "delete_time"?
		protected $has_is_delete	= true;		// имеет ли модель служебное поле "is_delete"?
		protected $struct			= array();	// массив, в потомках определяем структуру данных модели
		protected $special			= array();	// массив, автоматически формируется, определяем структуру служебных данных
		protected $relations		= array();	// массив, определяем отношения между моделями
		protected $rules			= 0;		// параметр для API-методов, задающий правила обработки данных
		
		public	  $id				= 0;		// ID поседней сохранненой модели
		public    $last_sql_query	= '';		// последний выполенный SQL-запрос модели (для отладки)
		
		/**
		 * Инициализация структуры данных модели, а также 
		 * её служебных данных и отношений с другими моделями
		 */
		abstract protected function __init_model();
		
		final public function __init() {
			
			// определяем имя текущей модели
			$this->name = preg_replace('/^(.+)_model$/', '$1', get_class($this));
			
			// определяем структуру и связи отношений текущей модели
			$this->__init_model();
		
			// определяем структуру служебных полей
			$this->special['id'] = array(
				'type'	=> self::T_INT,
				'index'	=> self::I_PRIMARY
			);
			if($this->has_is_delete) {
				$this->special['is_delete'] = array(
					'type'	=> self::T_INT
				);
			}
			if($this->has_insert_time) {
				$this->special['insert_time'] = array(
					'type'	=> self::T_DATE
				);
			}
			if($this->has_update_time) {
				$this->special['update_time'] = array(
					'type'	=> self::T_DATE
				);
			}
			if($this->has_delete_time) {
				$this->special['delete_time'] = array(
					'type'	=> self::T_DATE
				);
			}
			
			// определям поля отношений
			if(!empty($this->relations)) {
				foreach($this->relations as $i => &$data) {
					if(!empty($data['type']) && $data['type'] == self::R_ASSOCIATE && !empty($data['field'])) {
						$this->special[$data['field']] = array(
							'type'		=> self::T_INT,
							'default'	=> 0
						);
					}
				}
			}
			
		}
		
		/**
		 * callback-метод для обработки входящих условий в модель 
		 * до получаения данных из модели методами типа "get_"
		 */
		protected function __before_get($data) {
			return $data;
		}
		
		/**
		 * callback-метод для обработки выходящих данных из модели
		 * методами типа "get_"
		 */
		protected function __after_get($data) {
			return $data;
		}
		
		/**
		 * callback-метод для обработки входящих условий в модель 
		 * для сохранения данных в модели методом "save"
		 */
		protected function __before_save($data) {
			return $data;
		}
		
		/**
		 * callback-метод для действий после сохранения
		 * данных в моделм методом "save"
		 */
		protected function __after_save($data) {
		}
		
		protected function __after_insert($data) {
		}
		
		protected function __after_update($data) {
		}
		
		/**
		 * Возвращает несколько запесей модели целиком или с $fields полями 
		 * по фильтрации $conds в кол-ве $count и страницей $page
		 */
		public function get_all($fields = null, $conds = null, $order = null, $count = 0, $page = 0, $rules = null) {
			// получаем правила обработки
			$this->rules = $rules;
			
			// основной SQL-запрос
			$sql = join(' ', array_filter(array(
				'SELECT '.$this->get_sql_select($fields),
				'FROM '.$this->get_table(),
				$this->get_sql_where($conds),
				$this->get_sql_order($order),
				$this->get_sql_limit($count, $page)
			)));
			$this->last_sql_query = $sql;
			$data = $this->_sql->get_all($sql);
			// var_dump($sql);
			// die('!');
			if(empty($data)) {
				return false;
			}
			
			// обработка callback-функцией
			if(!($this->rules & self::R_WITHOUT_AFTER_GET)) {
				foreach($data as $i => &$item) {
					$item = $this->__after_get($item);
				}
			}
			
			// ассоциативные отношения
			if(!empty($this->relations) && !($this->rules & self::R_WITHOUT_RECURSIVE)) {
				foreach($data as $i => &$item) {
					$item = $this->get_associate($item);
				}
			}
			
			$this->rules = 0;
			
			return $data;
		}
		
		/**
		 * Возвращает одну запись модели целиком или с $fields полями по фильтрации $conds
		 */
		public function get_row($fields = null, $conds = null, $order = null, $rules = null) {
			// получаем особые правила
			$this->rules = $rules;
			
			$sql = join(' ', array_filter(array(
				'SELECT '.$this->get_sql_select($fields),
				'FROM '.$this->get_table(),
				$this->get_sql_where($conds),
				$this->get_sql_order($order),
				'LIMIT 1'
			)));
			$this->last_sql_query = $sql;
			$data = $this->_sql->get_row($sql);
			
			// обработка callback-функцией
			if(!($this->rules & self::R_WITHOUT_AFTER_GET)) {
				$data = $this->__after_get($data);
			}
			
			// ассоциативные отношения
			if(!empty($this->relations) && !($this->rules & self::R_WITHOUT_RECURSIVE)) {
				$data = $this->get_associate($data);
			}
			
			$this->rules = 0;
			
			return $data;
		}
		
		/**
		 * Возвращает одну запись модели целиком или с $fields полями по фильтрации $conds
		 */
		public function get_one($field = null, $conds = null, $order = null, $rules = null) {
			// получаем особые правила
			$this->rules = $rules;
			
			// формируем SQL-запрос
			$sql = join(' ', array_filter(array(
				'SELECT '.$this->get_sql_select(array($field)),
				'FROM '.$this->get_table(),
				$this->get_sql_where($conds),
				$this->get_sql_order($order),
				'LIMIT 1'
			)));
			
			$this->last_sql_query = $sql;
			$data = $this->_sql->get_row($sql);
			
			// обработка callback-функцией
			if(!($this->rules & self::R_WITHOUT_AFTER_GET)) {
				$data = $this->__after_get($data);
			}
			
			// ассоциативные отношения
			if(!empty($this->relations) && !($this->rules & self::R_WITHOUT_RECURSIVE)) {
				$data = $this->get_associate($data);
			}
			
			$this->rules = 0;
			
			return array_shift($data);
		}
		
		/**
		 * Возвращает одну запись модели целиком или с $fields полями по ID модели
		 */
		public function get_item($id = 0, $rules = null) {
			// получаем особые правила
			$this->rules = $rules;
			
			$sql = join(' ', array_filter(array(
				'SELECT *',
				'FROM '.$this->get_table(),
				$this->get_sql_where(array(
					'id' => (int)$id
				)),
				'LIMIT 1'
			)));
			$this->last_sql_query = $sql;
			$data = $this->_sql->get_row($sql);
			
			// обработка callback-функцией
			if(!($this->rules & self::R_WITHOUT_AFTER_GET)) {
				$data = $this->__after_get($data);
			}
			
			// ассоциативные отношения
			if(!empty($this->relations) && !($this->rules & self::R_WITHOUT_RECURSIVE)) {
				$data = $this->get_associate($data);
			}
			
			$this->rules = 0;
			
			return $data;
		}
		
		/**
		 * Возвращает количество записей в моделе
		 */
		public function get_count($conds = null, $rules = null) {
			// получаем особые правила
			$this->rules = $rules;
			
			$sql = join(' ', array_filter(array(
				'SELECT COUNT(*)',
				'FROM '.$this->get_table(),
				$this->get_sql_where($conds),
			)));
			$this->last_sql_query = $sql;
			$count = (int)$this->_sql->get_one($sql);
			
			$this->rules = 0;
			
			return $count;
		}
		
		/**
		 * Сохраняем данные модели
		 */
		public function save($data = null, $rules = null) {
			// var_dump($data);
			// die('!'); 
			if(empty($data) || !is_array($data)) {
				return false;
			}
			
			if(!($rules & self::R_WITHOUT_BEFORE_SAVE)) {
				$data = $this->__before_save($data);
			}
			
			$id		= 0;
			$fields = array();
			foreach($data as $field => $value) {
				if($field == 'id') {
					$id = (int)$value;
				} elseif($this->isset_field($field)) {
					$fields[] = '`'.trim($field).'` = '.$this->format_field($field, $value);
				}
			}
			
			if(empty($fields)) return false;
			// $sql = 'INSERT INTO '.$this->get_table().' SET '.join(', ', $fields);
			// var_dump($data);
			// var_dump($sql); 
			// die('!');
			if(empty($id)) {
				$sql = 'INSERT INTO '.$this->get_table().' SET '.join(', ', $fields);
				$this->last_sql_query = $sql;
				if($this->_sql->query($sql)) {
					$id			= (int)mysql_insert_id();
					$this->id	= (int)$id;
					$data['id']	= (int)$id;
					if(!($rules & self::R_WITHOUT_AFTER_SAVE)) {
						$this->__after_insert($data);
					}
				}
			} else {
				$sql = 'UPDATE '.$this->get_table().' SET '.join(', ', $fields).', update_time = NOW() WHERE `id` = '.(int)$id;
				$this->last_sql_query = $sql;
				if(!$this->_sql->query($sql)) {
					$id			= 0;
				} else {
					$this->id	= (int)$id;
					$data['id']	= (int)$id;
					if(!($rules & self::R_WITHOUT_AFTER_SAVE)) {
						$this->__after_update($data);
					}
				}
			}
			
			if(!($rules & self::R_WITHOUT_AFTER_SAVE)) {
				$this->__after_save($data);
			}
			
			return true;
		}
		
		public function delete($id = 0) {
			return $this->save(array(
				'id'			=> (int)$id,
				'is_delete'		=> 1,
				'delete_time'	=> time()
			));
		}
		
		// получаем имя текущей или переданной в качестве параметра модели
		protected function get_table($name = null) {
			return '`'.$this->_conf->get('db/pref').'_'.($name ? $name : $this->name).'`';
		}
		
		private function format_field($field, $value) {
			if(isset($this->struct[trim($field)]['type'])) {
				return $this->format_field_inner($this->struct[trim($field)]['type'], $value);
			}
			if(isset($this->special[trim($field)]['type'])) {
				return $this->format_field_inner($this->special[trim($field)]['type'], $value);
			}
			return null;
		}
		
		private function format_field_inner($type, $value) {
			switch($type) {
				case self::T_INT:		return (int)$value;
				case self::T_DATE:		return '"'.date('Y-m-d H:i:s', strtotime($value) > 0 ? strtotime($value) : (int)$value).'"';
				case self::T_STRING:	return $this->_sql->escape($value); 
				case self::T_BOOL:		return (int)((bool)$value);
				case self::T_TEXT:		return $this->_sql->escape($value);
			}
			return null;
		}
		
		// проверяем поле на существование в структуре
		public function isset_field($field) {
			return
				array_key_exists(trim($field), $this->struct) || 
				array_key_exists(trim($field), $this->special);
		}
		
		// формируем часть SQL-запроса SELECT в соответствии со списом полей
		private function get_sql_select($fields = array()) {
			if(empty($fields)) return '*';
			if(is_string($fields)) {
				$fields = explode(',', $fields);
			}
			
			if(!empty($this->relations)) {
				foreach($this->relations as $i => &$rel) {
					if(!empty($rel['type']) && $rel['type'] == self::R_ASSOCIATE) {
						$fields[] = $rel['field'];
					}
				}
			}
			
			foreach($fields as $i => &$field) {
				if(!$this->isset_field($field)) {
					unset($fields[$i]);
				} else {
					$field = '`'.trim($field).'`';
				}
			}
			return (!empty($fields)) ? join(', ', $fields) : '*';
		}
		
		// формируем часть SQL-запроса WHERE в соответствии с массивом условий
		private function get_sql_where($conds = null) {
			if(!empty($conds) && !($this->rules & self::R_WITHOUT_BEFORE_GET)) {
				$conds = $this->__before_get($conds);
			}
			
			$conds_array = array();
			if(!empty($conds)) {
				foreach($conds as $field => $value) {
					if($this->isset_field($field) && $value !== null) {
						$conds_array[] = '`'.trim($field).'` = '.$this->format_field($field, $value);
					}
				}
			}
			
			if($this->has_is_delete) {
				$conds_array[] = '`is_delete` = 0';
			}
			
			return (!empty($conds_array)) ? 'WHERE '.join(' AND ', $conds_array) : '';
		}
		
		// формируем часть SQL-запроса ORDER BY в соответствии с массивом полей
		private function get_sql_order($order = null) {
			if(empty($order)) return '';
			
			$order_array = array();
			foreach($order as $field => $direction) {
				if($this->isset_field($field)) {
					if(in_array($direction, array(0, 'asc'), true)) {
						$order_array[] = '`'.$field.'` ASC';
					} elseif(in_array($direction, array(1, 'desc'), true)) {
						$order_array[] = '`'.$field.'` DESC';
					}
				}
			}
			return (!empty($order_array)) ? 'ORDER BY '.join(', ', $order_array) : '';
		}
		
		// формируем часть SQL-запроса LIMIT
		private function get_sql_limit($count, $page) {
			$count	= (int)$count;
			$page	= (int)$page;
			
			return (!empty($count)) ? 'LIMIT '.($page * $count).', '.$count : '';
		}
		
		private function get_associate($item) {
			foreach($this->relations as $i => &$rel) {
				if(!empty($rel['type']) && $rel['type'] == self::R_ASSOCIATE && class_exists($rel['model'].'_model')) {
					if(!empty($rel['field']) && !empty($item[$rel['field']])) {
						$rec_data = $this->$rel['model']->get_row(
							array_keys($this->$rel['model']->struct),
							array('id' => (int)$item[$rel['field']])
						);
						if(!empty($rec_data)) {
							foreach($rec_data as $field => &$value) {
								$item[(!empty($rel['name']) ? $rel['name'] : $rel['model']).'_'.$field] = $value;
							}
						}
					}
				}
			}
			return $item;
		}
	
	}
?>