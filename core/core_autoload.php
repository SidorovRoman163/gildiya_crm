<?php
	/**
	 * "Волшебная" функция для подгрузки файлов классов
	 *  во время их первого непосредственного использования
	 */
	function __autoload($class) {
		$class  = strtolower($class);
		
		// классы ядра
		if(__autoload_check($class, CORE)) return true;
		if(__autoload_check($class, ROOT.'app'.DS)) return true;
		if(__autoload_check($class, ROOT.'api'.DS)) return true;
		
		// контроллеры приложения
		if(preg_match('/^([a-z]+)_controller$/i', $class, $match)) {
			$app = core_application::$app_name;
			if(__autoload_check($class, ROOT.$match[1].DS)) return true;
			if(__autoload_check($app.'_'.$class, ROOT.$app.DS.'modules'.DS.$match[1].DS)) return true;
		}
		
		// компоненты ядра
		if(preg_match('/^([a-z]+)_component$/i', $class, $match)) {
			if(__autoload_check($class, CORE.'components'.DS)) return true;
		}
		
		// основные модели приложения
		if(preg_match('/^([a-z]+)(_[a-z]+)?(_[a-z]+)?_model$/i', $class, $match)) {
			if(__autoload_check('app_'.$class, ROOT.'app'.DS.'modules'.DS.$match[1].DS)) return true;
			$app = core_application::$app_name;
			if(__autoload_check($app.'_'.$class, ROOT.$app.DS.'modules'.DS.$match[1].DS)) return true;
		}
		
		return false;
	}
	
	// функция подгрузка классов
	function __autoload_check($class, $path) {
		if(file_exists($path.$class.'.php')) {
			require_once $path.$class.'.php';
			return true;
		}
		return false;
	}
?>