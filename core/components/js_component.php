<?php
	class js_component extends core_component {
	
		private $data = array();
	
		public function assign($path, $external = false) {
			
			if(!empty($external)) {
				$this->data[] = array(
					'url' => $path
				);
				return true;
			}
			
			// получаем путь к файлу шаблона
			$parts = explode('/', $path);
			$pathes = array();
			if(count($parts) > 1) {
				$pathes[] = array(
					'src' => '/'.self::$app_name.'/modules/'.$parts[0].'/sources/js/'.$parts[1].'.js',
					'abs' => ROOT.self::$app_name.DS.'modules'.DS.$parts[0].DS.'sources'.DS.'js'.DS.$parts[1].'.js'
				);
			}
			$pathes[] = array(
				'src' => '/'.self::$app_name.'/sources/js/'.$path.'.js',
				'abs' => ROOT.self::$app_name.DS.'sources'.DS.'js'.DS.$path.'.js'
			);
			foreach($pathes as $path) {
				if(file_exists($path['abs'])) {
					$this->data[] = $path;
					return true;
				}
			}
			
			return false;
		}
		
		public function generate() {
			
			if(empty($this->data)) return '';
			$included = array();
			
			$html = '';
			foreach($this->data as $i => &$path) {
				if(!empty($path['url'])) {
					$html .= '<script type="text/javascript" src="'.$path['url'].'"></script>';
					continue;
				}
				if(!in_array($path['src'], $included)) {
					$html .= '<script type="text/javascript" src="'.$path['src'].'?_='.filemtime($path['abs']).'"></script>';
					$included[] = $path['src'];
				}
			}
			
			return $html;
		}
		
	}
?>