<?php
	class format_component extends core_component {
		
		public function declText($count, $one, $many, $vmany, $is_dec = true, $just_text = false) {
			if($is_dec && $count > 10 && $count < 20) return (empty($just_text)?$count:'').' '.$vmany;
			if($count % 10 == 1) return (empty($just_text)?$count:'').' '.$one;
			elseif(in_array($count % 10, array(2,3,4))) return (empty($just_text)?$count:'').' '.$many;
			else return (empty($just_text)?$count:'').' '.$vmany;
		}
		
		public function rusText2mins($text) {
			$patterns = array(
				array('pattern' => '([0-9]+)\s?нед(ель|ели|еля)?',	'mins' => 2400),
				array('pattern' => '([0-9]+)\s?(дн|дня|дней|день)',	'mins' => 480),
				array('pattern' => '([0-9]+)\s?час(а|ов)?',			'mins' => 60),
				array('pattern' => '([0-9]+)\s?мин(ут|ута)?',		'mins' => 1)
			);
			$mins = 0;
			foreach($patterns as $item) {
				if(preg_match('/'.$item['pattern'].'/iuU', $text, $match)) {
					$mins += $item['mins'] * (int)$match[1];
				}
			}
			return $mins;
		}
		
		public function mins2rusText($mins = 0) {
			if(empty($mins) || (int)$mins <= 0) {
				return '';
			}
			
			$patterns = array(
				array('mins' => 2400,	'titles' => array('неделя', 'недели', 'недель')),
				array('mins' => 480,	'titles' => array('день', 'дня', 'дней')),
				array('mins' => 60,		'titles' => array('час', 'часа', 'часов')),
				array('mins' => 1,		'titles' => array('минута', 'минуты', 'минут'))
			);
			$text = array();
			foreach($patterns as $item) {
				if((int)$mins >= $item['mins']) {
					$value_part = (int)floor((int)$mins / $item['mins']);
					$text[] = $this->declText($value_part, $item['titles'][0], $item['titles'][1], $item['titles'][2]);
					$mins = (int)$mins - ($value_part * $item['mins']);
				}
			}
			return join(' ', $text);
		}
		
		public function num2rusText($num) {
			$m = array(
				array('ноль'),
				array('-','один','два','три','четыре','пять','шесть','семь','восемь','девять'),
				array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать','пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать'),
				array('-','-','двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят','восемьдесят','девяносто'),
				array('-','сто','двести','триста','четыреста','пятьсот','шестьсот','семьсот','восемьсот','девятьсот'),
				array('-','одна','две')
			);

			$r = array(
				array('...ллион','','а','ов'), 
				array('тысяч','а','и',''),
				array('миллион','','а','ов'),
				array('миллиард','','а','ов'),
				array('триллион','','а','ов'),
				array('квадриллион','','а','ов'),
				array('квинтиллион','','а','ов')		  
			);
			
			$num = (int)$num;
			if($num == 0) return $m[0][0]; 
			$o = array(); 
			
			foreach(array_reverse(str_split(str_pad($num,ceil(strlen($num)/3)*3,'0',STR_PAD_LEFT),3)) as $k => $p){
				$o[$k] = array();
				
				foreach($n = str_split($p) as $kk => $pp)
				if(!$pp) continue; else
				switch($kk){
					case 0:$o[$k][] = $m[4][$pp];break;
					case 1:if($pp == 1) {$o[$k][] = $m[2][$n[2]]; break 2;} else $o[$k][] = $m[3][$pp]; break;
					case 2:if(($k == 1) && ($pp <= 2)) $o[$k][] = $m[5][$pp]; else $o[$k][] = $m[1][$pp]; break;
				} $p *= 1; if(!$r[$k]) $r[$k] = reset($r);

				# Алгоритм, добавляющий разряд, учитывающий окончание руского языка
				if($p&&$k) switch(true){
					case preg_match("/^[1]$|^\d*[0,2-9][1]$/",$p):$o[$k][]=$r[$k][0].$r[$k][1]; break;
					case preg_match("/^[2-4]$|\d*[0,2-9][2-4]$/",$p):$o[$k][]=$r[$k][0].$r[$k][2]; break;
					default: $o[$k][]=$r[$k][0].$r[$k][3]; break;
				}$o[$k]=implode(' ',$o[$k]);
			}
			 
			return implode(' ', array_reverse($o));
		}
		
		public function phone($phone) {
			if(empty($phone)) {
				return '';
			}
			
			$phone = preg_replace('/[^0-9]+/', '', $phone);
			
			switch(strlen($phone)) {
				case 10:
					return '+7 ('.substr($phone, 0, 3).') '.substr($phone, 3, 3).'-'.substr($phone, 6, 2).'-'.substr($phone, 8, 2);
				case 11:
					return '+7 ('.substr($phone, 1, 3).') '.substr($phone, 4, 3).'-'.substr($phone, 7, 2).'-'.substr($phone, 9, 2);
			}
			return '';
		}
		
		public function price($price) {
			return number_format(round($price), 0	, '.', ' ').' руб.';
		}
		
		public function fileSize($file_size = 0) {
			if(empty($file_size)) {
				return $file_size.' б';
			}
			
			$itr = 0;
			$file_size = (int)$file_size;
			$file_dims = array('б', 'Кб', 'Мб');
			foreach($file_dims as $size) {
				if($file_size < 1024) {
					break;
				}
				$file_size /= 1024;
				$itr++;
			}
			
			return round($file_size, 1).' '.$file_dims[$itr];
		}
		
	}
?>