<?php
	class yadisk_component extends core_component { 
		
		private $login;
		private $pass;
		private $url;
		
		public function __init() {
			$this->login	= 'lada-sport-disk@yandex.ru';
			$this->pass		= 'CVJ05938gh038g2t4fog9u';
			$this->url		= 'https://webdav.yandex.ru';
		}
		
		/**
		 * Получение данных по директории
		 */
		public function get_dir($path = '/') {
			$xml = $this->request('PROPFIND', $path, array('Depth: 1'));
			if(empty($xml)) {
				return false;
			}
			$list = array();
			foreach($xml->response as $item) {
				if($item->href != $path) {
					if(count($item->propstat->prop->resourcetype->children())) {
						$list[] = array(
							'is_dir'		=> true,
							'is_file'		=> false,
							'title'			=> $item->propstat->prop->displayname->__toString(),
							'href'			=> $item->href->__toString()
						);
					}
					if($item->propstat->prop->getcontenttype->__toString()) {
						$list[] = array(
							'is_dir'		=> false,
							'is_file'		=> true,
							'title'			=> $item->propstat->prop->displayname->__toString(),
							'type'			=> $item->propstat->prop->getcontenttype->__toString(),
							'size'			=> (int)$item->propstat->prop->getcontentlength->__toString(),
							'create_date'	=> strtotime($item->propstat->prop->creationdate->__toString()),
							'lastmod_date'	=> strtotime($item->propstat->prop->getlastmodified->__toString()),
							'etag'			=> $item->propstat->prop->getetag->__toString(),
							'href'			=> $item->href->__toString()
						);
					}
				}
			}
			return $list;
		}
		
		/**
		 * Получаение данных по файлу
		 */
		public function get_file_info($path) {
			$xml = $this->request('PROPFIND', $path, array('Depth: 1'));
			if(empty($xml)) {
				return false;
			}
			return array(
				'title'			=> $xml->response->propstat->prop->displayname->__toString(),
				'type'			=> $xml->response->propstat->prop->getcontenttype->__toString(),
				'size'			=> (int)$xml->response->propstat->prop->getcontentlength->__toString(),
				'create_date'	=> strtotime($xml->response->propstat->prop->creationdate->__toString()),
				'lastmod_date'	=> strtotime($xml->response->propstat->prop->getlastmodified->__toString()),
				'etag'			=> $xml->response->propstat->prop->getetag->__toString(),
				'href'			=> $xml->response->href->__toString()
			);
		}
		
		/**
		 * Запрос на получение превью
		 */
		public function get_preview($path) {
			$ch = curl_init($this->url.$path);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET'); 
			curl_setopt($ch, CURLOPT_PORT, 443); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Authorization: Basic '.base64_encode($this->login.':'.$this->pass)
			));
			$preview	= curl_exec($ch);
			$code		= (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			
			if($code == 200) {
				return $preview;
			}
			return false;
		}
		
		/**
		 * Метод скачивания файла
		 */
		public function download($path) {
			
			// получение свойств файла
			$xml = $this->request('PROPFIND', $path, array('Depth: 1'));
			if(empty($xml)) {
				$this->_url->referer();
			}
			if(empty($xml->response[0]) || empty($xml->response[0]->propstat->prop->getcontentlength)) {
				$this->_url->referer();
			}
			$size = (int)$xml->response[0]->propstat->prop->getcontentlength->__toString();
			$name = $xml->response[0]->propstat->prop->displayname->__toString();
			
			set_time_limit(0);
			$bytes = 4 * 1024 * 1024;
			$i = 0;
			
			header("Pragma: public"); 
			header("Expires: 0"); 
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
			header("Cache-Control: private", false);
			header("Content-Type: application/binary"); 
			header("Content-Disposition: attachment; filename=".urlencode($name)); 
			header("Content-Transfer-Encoding: binary"); 
			header("Content-Length: ".$size);
			flush();
			
			// скачивание файла и передача по частям
			do {
				$headers = array();
				$headers[] = 'Authorization: Basic '.base64_encode($this->login.':'.$this->pass);
				$headers[] = 'Range: bytes='.($i * $bytes).'-'.(($i + 1) * $bytes - 1);
				
				$ch = curl_init($this->url.$path);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET'); 
				curl_setopt($ch, CURLOPT_PORT, 443); 
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				
				$res	= curl_exec($ch);
				$code	= (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
				
				echo $res; 
				flush();
				$i++;
			} while($code == 206);
			
			die();
		}
		
		private function request($method, $path = '/', $headers = array()) {
			$headers[] = 'Authorization: Basic '.base64_encode($this->login.':'.$this->pass);
			
			$ch = curl_init($this->url.$path);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); 
			curl_setopt($ch, CURLOPT_PORT, 443); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
			$res = curl_exec($ch);
			$res = str_replace('d:', '', $res);
			return new SimpleXMLElement($res);
		}
		
	}
?>