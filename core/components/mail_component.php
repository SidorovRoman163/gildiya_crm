<?php
	class mail_component extends core_component {
		
		public $handler;
		
		public function __init() {
			
			if (!file_exists(LIBS.'class.phpmailer.php')) {
				die('Not found: '.LIBS.'class.phpmailer.php');
			}
			
			require_once LIBS.'class.phpmailer.php';
			$this->handler = new PHPMailer();
			
			$host	= $this->_conf->get('settings/email_smtp_server');
			$port	= $this->_conf->get('settings/email_smtp_port');
			$login	= $this->_conf->get('settings/email_login');
			$pass	= $this->_conf->get('settings/email_pass');
			$this->smtp($host, $port, $login, $pass, true);
			
			$email	= $this->_conf->get('settings/email_address');
			$name	= $this->_conf->get('settings/email_name');
			$this->from($email, $name);
			
			$this->handler->AddReplyTo($email, $name);
			$this->subject('Письмо с GRM-системы');
		}
		
		public function smtp($host, $port, $login, $pass, $use_ssl = false) {
			$this->handler->IsSMTP();
			$this->handler->Host			= $host;
			$this->handler->SMTPAuth		= true;
			$this->handler->SMTPKeepAlive	= true;
			if(!empty($use_ssl)) {
				$this->handler->SMTPSecure	= 'ssl';
			}
			$this->handler->Port			= (int)$port;
			$this->handler->Username		= $login;
			$this->handler->Password		= $pass;
		}
		
		public function from($email, $name = '') {
			$this->handler->Sender = '';
			$this->handler->ClearReplyTos();
			$this->handler->ClearAllRecipients();
			$this->handler->SetFrom($email, $name, true);
		}
		
		public function subject($str) {
			$this->handler->Subject = $str;
		}
		
		// -- отправить письмо
		public function send_mail($to, $letter, $subject = false, $attaches = false, $config = false) {
			$this->clear();
			
			if (!empty($config) && is_array($config)) {
				foreach ($config as $conf_h => $conf_v) {
					switch ($conf_h) {
						case 'From':
							$this->handler->SetFrom($conf_v[0], $conf_v[1]);
							break;
						case 'ReplyTo':
							$this->handler->ClearReplyTos();
							$this->handler->AddReplyTo($conf_v[0], $conf_v[1]);
							break;
					}
				}
			}
			
			// -- перекрываем тему письма
			if (!empty($subject)) {
				$this->handler->Subject = $subject;
			}
			
			// -- формируем тело письма
			$this->handler->MsgHTML($letter);
			$this->handler->AltBody = strip_tags($letter);
			
			// -- добавляем прикрепленные файлы
			if (!empty($attaches)) {
				if (is_string($attaches)) {
					if (file_exists($attaches)) {
						$this->handler->AddAttachment($attaches);
					}
				} elseif (is_array($attaches)) {
					foreach ($attaches as $name => $path) {
						if (file_exists($path)) {
							$this->handler->AddAttachment($path, $name);
						}
					}
				}
			}
			
			// добавляем полтзователей
			$tos = explode(',', $to);
			foreach($tos as $to) {
				$this->handler->AddAddress(trim($to));
			}
			
			return $this->handler->Send();
		}
		
		// -- очистить от предыдущей отправки
		public function clear() {
			$this->handler->ClearAddresses();
			$this->handler->ClearAttachments();
		}
		
		/**
		 * Метод обработки HTML-контента письма перед отправкой
		 */
		public function html($text) {
			
			// H1
			if(preg_match_all('/<h1>(.+?)<\/h1>/iu', $text, $matches)) {
				foreach($matches[0] as $i => &$str) {
					$text = str_replace($str, '<h1 style="font-size:18px;font-weight:normal;margin:0 0 16px 0;">'.$matches[1][$i].'</h1>', $text);
				}
			}
			
			// TABLE
			if(preg_match_all('/<table(.*?)>(.+?)<\/table>/iu', $text, $matches)) {
				foreach($matches[1] as $i => &$str) {
					if(preg_match('/style=\"(.+?)\"/iU', $str, $match)) {
						$text = str_replace($matches[0][$i], '<table'.str_replace($match[0],'',$matches[1][$i]).' style="'.$match[1].'margin:0 12px;">'.$matches[2][$i].'</table>', $text);
					} else {
						$text = str_replace($matches[0][$i], '<table'.$matches[1][$i].' style="margin:0 12px;">'.$matches[2][$i].'</table>', $text);
					}
				}
			}
			
			// TH
			if(preg_match_all('/<th(.*?)>(.+?)<\/th>/iu', $text, $matches)) {
				foreach($matches[1] as $i => &$str) {
					if(preg_match('/style=\"(.+?)\"/iU', $str, $match)) {
						$text = str_replace($matches[0][$i], '<th'.str_replace($match[0],'',$matches[1][$i]).' style="'.$match[1].'font-size:14px;font-weight:normal;color:#787878;padding:2px 8px;">'.$matches[2][$i].'</th>', $text);
					} else {
						$text = str_replace($matches[0][$i], '<th'.$matches[1][$i].' style="font-size:14px;font-weight:normal;color:#787878;padding:2px 8px;">'.$matches[2][$i].'</th>', $text);
					}
				}
			}
			
			// TD
			if(preg_match_all('/<td(.*?)>(.+?)<\/td>/iu', $text, $matches)) {
				foreach($matches[0] as $i => &$str) {
					$text = str_replace($str, '<td'.$matches[1][$i].' style="font-size:14px;padding:2px 8px;">'.$matches[2][$i].'</td>', $text);
				}
			}
			
			// A
			if(preg_match_all('/<a(.*?)>(.+?)<\/a>/iu', $text, $matches)) {
				foreach($matches[0] as $i => &$str) {
					$text = str_replace($str, '<a'.$matches[1][$i].' style="color:red;">'.$matches[2][$i].'</a>', $text);
				}
			}
			
			return $text;
		}
		
	}
?>