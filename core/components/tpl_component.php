<?php
	class tpl_component extends core_component {
	
		private $glob	= array();
		private $data	= array();
		private $buffer = '';
	
		public function assign($label, $value = null) {
			
			if(!empty($label) && is_array($label) && $value === null) {
				foreach($label as $arr_label => &$arr_value) {
					$this->glob[$arr_label] = $arr_value;
				}
				return true;
			}
			
			if(!empty($label) && !empty($value)) {
				$this->glob[$label] = $value;
				return true;
			}
			
			return false;
		}
		
		public function get($label) {
			if(isset($this->glob[$label]) && is_string($this->glob[$label])) {
				return $this->glob[$label];
			}
			return false;
		}
		
		public function render($path = false, $data = null, $label = null) {
			
			// получаем путь к файлу шаблона
			if(empty($path)) {
				$tpl_parts[0] = self::$uri_controller;
				$tpl_parts[1] = self::$uri_method;
			} else {
				$tpl_parts	= explode('/', $path);
				if(count($tpl_parts) == 1) {
					$tpl_parts[1] = $tpl_parts[0];
					$tpl_parts[0] = self::$uri_controller;
				}
			}
			
			// формирование путей
			switch($tpl_parts[0]) {
				case 'layouts': $tpl_source_path = ROOT.self::$app_name.DS.'layouts'.DS.$tpl_parts[1].'.tpl'; break;
				default:		$tpl_source_path = ROOT.self::$app_name.DS.'modules'.DS.$tpl_parts[0].DS.'views'.DS.$tpl_parts[1].'.tpl'; break;
			}
			$tpl_cache_path		= CACHE.'tpl'.DS.md5($tpl_source_path).'.ctpl';
			
			// проверяем на наличие исходного файла шаблона
			if(!file_exists($tpl_source_path)) {
				die($tpl_source_path);
				return false;
			}
			
			// актуальность скомпилированного шаблона
			if(!file_exists($tpl_cache_path) || filemtime($tpl_cache_path) < filemtime($tpl_source_path)) {
			
				$this->buffer = file_get_contents($tpl_source_path);
				$this->obfuscate($this->buffer);
				$this->buffer_render();

				file_put_contents($tpl_cache_path, $this->buffer);
			}
			
			// слияние глобального и локального массива
			$this->data = (!empty($data)) ? array_merge($this->glob, (array)$data) : $this->glob;
			
			// рендер
			ob_start();
			require $tpl_cache_path;
			$html = ob_get_clean();
			
			// обфускация отрендеренного шаблона
			$this->obfuscate($html);
			
			// обнуление локального массива
			$this->data = array();
			
			// особая ситуация: метка "content"
			if($data === null && $label === null && $tpl_parts[0] !== 'layouts') {
				$this->assign('content', $html);
				return true;
			}
			
			// присоединение отрендеренного шаблона
			if(!empty($label)) {
				$this->assign($label, $html);
				return true;
			}
			
			// возврат отрендеренного шаблона
			return $html;
		}
		
		private function obfuscate(&$text) {
			$text = preg_replace('/\s+/', ' ',  $text);
			$text = str_replace('> <',    '><', $text);
		}
		
		private function buffer_render() {
			
			// порядок следования правил имеет значение
			$this->buffer = preg_replace(array(
				'/\{foreach (\$[a-z0-9_.]+) as (\$[a-z0-9_]+)\}/ie',	// #1 открытие цикла foreach
				'/\{\/foreach\}/i',										// #2 закрытие цикла foreach
				'/\{if (\$[a-z0-9_.]+)\}/ie',							// #3.1 открытие if - существование переменной
				'/\{if \!(\$[a-z0-9_.]+)\}/ie',							// #3.2 открытие if - существование переменной
				'/\{if (.+?)\}/ie',										// #3.3 открытие if
				'/\{else\}/i',											// #4 else
				'/\{\/if\}/i',											// #5 закрытие if
				'/\{(\$[a-z0-9_.\(\)]+)\}/ie'							// #6 строковые переменные
			), array(
				'$this->buffer_render_vars(\'$1\', \'<?php if(!empty(%%%) && is_array(%%%) && count(%%%) > 0) { \'.str_replace(\'.\', \'_\', \'$2\').\'_count = count(%%%); foreach(%%% as \'.str_replace(\'.\', \'_\', \'$2\').\'_iterator => &$2) { ?>\')',
				'<?php } } ?>',
				'$this->buffer_render_vars(\'$1\', \'<?php if(!empty(%%%)) { ?>\')',
				'$this->buffer_render_vars(\'$1\', \'<?php if(empty(%%%)) { ?>\')',
				'$this->buffer_render_vars(\'$1\', \'<?php if(%%%) { ?>\')',
				'<?php } else { ?>',
				'<?php } ?>',
				'$this->buffer_render_vars(\'$1\', \'<?=(isset(%%%)?%%%:\\\'\\\');?>\')'
			), $this->buffer);
			
		}
		
		private function buffer_render_vars($text, $wrap = null, $decode = true) {
			
			$text = preg_replace(array(
				'/(\$([a-z0-9_.]+))\.even\(\)/i',		// #1 нечетные элементы массива
				'/(\$([a-z0-9_.]+))\.odd\(\)/i',		// #2 четные элементы массива
				'/(\$([a-z0-9_.]+))\.pos\(\)/i',		// #3 позиция элемента в массиве
				'/(\$([a-z0-9_.]+))\.first\(\)/i',		// #4 первый элемент в массиве
				'/(\$([a-z0-9_.]+))\.last\(\)/ie',		// #5 последний элемент в массиве
				'/(\$([a-z0-9_.]+))\.([a-z0-9_]+)/i',	// #6 вложенный элемент массива
				'/(\$([a-z0-9_.]+))/i'					// #7 обычная переменная
			), array(
				'##$2_iterator%2==0',
				'##$2_iterator%2==1',
				'##$2_iterator',
				'##$2_iterator==0',
				'$this->buffer_render_vars(\'$1\', \'##\'.str_replace(\'.\', \'_\', \'$2\').\'_iterator==##\'.str_replace(\'.\', \'_\', \'$2\').\'_count-1\', false)',
				'##$2[\'$3\']',
				'##this->data[\'$2\']'
			), $text);
			
			if($decode) {
				$text = str_replace('##', '$', $text);
			}
			
			return ($wrap) ? str_replace('%%%', $text, $wrap) : $text;
		}
	
	}
?>