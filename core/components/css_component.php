<?php
	class css_component extends core_component {
	
		private $data	= array();
	
		public function assign($path) {
			
			// получаем путь к файлу шаблона
			$parts = explode('/', $path);
			$pathes = array();
			if(count($parts) > 1) {
				$pathes[] = array(
					'src' => '/'.self::$app_name.'/modules/'.$parts[0].'/sources/css/'.$parts[1].'.css',
					'abs' => ROOT.self::$app_name.DS.'modules'.DS.$parts[0].DS.'sources'.DS.'css'.DS.$parts[1].'.css'
				);
			}
			$pathes[] = array(
				'src' => '/'.self::$app_name.'/sources/css/'.$path.'.css',
				'abs' => ROOT.self::$app_name.DS.'sources'.DS.'css'.DS.$path.'.css'
			);
			foreach($pathes as $path) {
				if(file_exists($path['abs'])) {
					// var_dump($path['abs']);
					$this->data[] = $path;
					return true;
				}
			}
			return false;
		}
		
		public function generate() {
			if(empty($this->data)) return '';
			
			$included = array();
			
			$html = '';
			foreach($this->data as $i => &$path) {
				// var_dump($path['abs']);
				if(!in_array($path['src'], $included)) {
					$html .= '<link rel="stylesheet" href="'.$path['src'].'?_='.filemtime($path['abs']).'" type="text/css" />';
					$included[] = $path['src'];
				}
			}
			
			return $html;
		}
		
	}
?>