<?php
	class file_component extends core_component {
		
		public $tmp_path;
		public $tmp_ext;
		
		private $image_path;
		
		public function load($file_path) {
			if(file_exists($file_path)) {
				require_once $file_path;
				return true;
			}
			
			return false;
		}
		
		public function upload_exists($file_name = false) {
			if(empty($file_name) || empty(self::$files[$file_name]['tmp_name'])) {
				return false;
			}
			if(!file_exists(self::$files[$file_name]['tmp_name'])) {
				return false;
			}
			if(!is_uploaded_file(self::$files[$file_name]['tmp_name'])) {
				return false;
			}
			if(!empty(self::$files[$file_name]['error'])) {
				return false;
			}
			if(empty(self::$files[$file_name]['size'])) {
				return false;
			}
			
			$this->tmp_path	= self::$files[$file_name]['tmp_name'];
			$this->tmp_ext	= array_shift(array_reverse(explode('.', self::$files[$file_name]['name'])));
			$this->tmp_ext	= $this->getExt(self::$files[$file_name]['name']);
			return true;
		}
		
		public function getExt($name) {
			return array_shift(array_reverse(explode('.', $name)));
		}
		
		public function upload_move($file_name = false, $move_file_path) {
			if(empty($move_file_path)) return false;
			if(empty($file_name) || empty(self::$files[$file_name]['tmp_name'])) return false;
			if(!file_exists(self::$files[$file_name]['tmp_name'])) return false;
			
			return move_uploaded_file(self::$files[$file_name]['tmp_name'], $move_file_path);
		}
		
		public function image_exists($path = false, $id = 0, $ext = 'jpg') {
			if(empty($path)) return false;
			
			$path = explode('/', $path);
			if(count($path) < 2) {
				return false;
			}
			
			$module = array_shift($path);
			$image_path = ROOT.self::$app_name.DS.'modules'.DS.$module.DS.'sources'.DS.join(DS, $path).DS.(int)$id.'.'.$ext;
			if(file_exists($image_path)) {
				$this->image_path = $image_path;
				return true;
			}
			
			return false;
		}
		
		public function image_tag($w = 0, $h = 0) {
			if(empty($this->image_path)) return '';
			
			$rel_path = '/'.str_replace(array(ROOT, DS), array('', '/'), $this->image_path);
			
			return '<img '.
				((int)$w > 0 ? 'width="'.(int)$w.'" '	: '').
				((int)$h > 0 ? 'height="'.(int)$h.'" '	: '').
				'src="'.$rel_path.'?_='.filemtime($this->image_path).'" />';
		}
		
		public function size($size) {
			if($size > 1024 * 1024) {
				return round($size / 1024 / 1024, 1).' Мб';
			} else if($size > 1024) {
				return round($size / 1024, 1).' Кб';
			} else {
				return $size.' б';
			}
		}
		
	}
?>