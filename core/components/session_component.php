<?php
	class session_component extends core_component {
		
		public function __init() {
			session_start();
		}
		
		public function is_set($label) {
			return isset($_SESSION[$label]);
		}
		
		public function set($label, $value) {
			if(!empty($label)) {
				$_SESSION[$label] = $value;
			}
		}
		
		public function get($label) {
			if(isset($_SESSION[$label])) {
				return $_SESSION[$label];
			}
			
			return null;
		}
		
		public function delete($label) {
			if(isset($_SESSION[$label])) {
				unset($_SESSION[$label]);
			}
		}
	
	}
?>