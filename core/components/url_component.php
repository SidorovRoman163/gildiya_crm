<?php
	class url_component extends core_component {
		
		/**
		 * Метод для редиректа на кукую-либо страницу,
		 */
		public function redirect($uri) {
			header('Location: '.$uri);
			die();
		}
		
		/**
		 * Метод для редиректа на предыдущую страницу,
		 * случае когда текущая станица является предудущей,
		 * редирект идет на главную страницу
		 */
		public function referer() {
			if(!empty($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !== $_SERVER['REQUEST_URI']) {
				$this->redirect($_SERVER['HTTP_REFERER']);
			}
			header('Location: /');
			die();
		}
		
		public function get_clear_url($is_without_lang = true) {
			return $_SERVER['REQUEST_URI'];
			return (empty($is_without_lang) ? '/'.self::$lang : '').preg_replace('%^/(ru|en)?(.*)$%', '$2', $_SERVER['REQUEST_URI']);
		}
	
	}
?>