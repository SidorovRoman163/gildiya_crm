<?php
	class cookie_component extends core_component {
		
		protected $salt = null;	

		public function __init() {
			$this->salt = $this->_conf->get('cookie/salt');
		}
		
		public function set($name, $value, $time = false) {			  
			if(!empty($name)) {
				$time = empty($time) ? 0 : time() + $this->validity($time); 				
				return setcookie($name, $value, $time, '/', HOST);
			}
			return false;
		}	
		
		public function delete_with_pattern($pattern) {
			if(empty($_COOKIE)) return false;
			
			foreach($_COOKIE as $name => $value) {
				if(preg_match($pattern, $name)) {
					$this->set($name, $value, 'reset');
				}
			}
			
			$this->set($name, $value, 'reset');           
		}
		
		public function reset($name, $value) {    			
			$this->set($name, $value, 'reset');           
		}  
		  
		public function get($pattern, &$value = null, &$matches = null) {
			if(empty($pattern)) return false;
			
			if(!empty($_COOKIE)) {
				foreach($_COOKIE as $name => $val) {
					if(preg_match($pattern, $name, $matches)) {
						$value = $val;
						return true;
					}
				}
			}
			return false;
		}
		
		private function validity($val) {
			switch($val) {
					case 'hour'  : $val = 3600; 	 break;  
					case 'day'   : $val = 86400; 	 break;
					case 'week'  : $val = 604800;    break;
					case 'month' : $val = 2592000;   break;
					case 'year'  : $val = 31536000;  break;
					case 'reset' : $val = -31536000; break;
					default		 : $val = (is_numeric($val)) ? $val : time();  
			}			
			return $val;
		}
		
	
	}	
?>