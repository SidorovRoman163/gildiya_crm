<?php
	class lang_component extends core_component { 
		
		private $data = array();
		
		public function load($lang_name) {
			
			$lang_file = CORE.'langs'.DS.'lang.'.$lang_name.'.php';
			if(!file_exists($lang_file)) {
				$lang_file = CORE.'langs'.DS.'lang.ru.php';
			}
			require_once $lang_file;
			
			if(!empty($lang)) {
				$this->data = $lang;
				foreach($this->data as $label => &$value) {
					$this->_tpl->assign('lang_'.$label, $value);
				}
			}
			
			$this->_tpl->assign('lang', $lang_name);
			$this->_tpl->assign('clear_url', $this->_url->get_clear_url());
		}
		
		public function get($label) {
			return (!empty($this->data[$label])) ? $this->data[$label] : '';
		}
		
	}
?>