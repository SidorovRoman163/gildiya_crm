<?php
	class image_component extends core_component {
		
		private $handler;
		private $width = 0;
		private $height = 0;
		private $image_path;
		
		public function open($image_path) {
			if(empty($image_path) || !file_exists($image_path)) {
				return false;
			}
			
			// перебираем функции чтения разных форматов (pjpeg: progressive-jpeg)
			if(!($this->handler = @imageCreateFromJPEG($image_path))) {
				if(!($this->handler = @imageCreateFromPNG($image_path))) {
					if(!($this->handler = @imageCreateFromGIF($image_path))) {
						return false;
					}
				}
			}
			$this->width		= ImageSX($this->handler);
			$this->height		= ImageSY($this->handler);
			$this->image_path	= $image_path;
			return true;
		}
		
		public function rotate($arc) {
			if(empty($this->handler)) {
				return false;
			}
			
			$background = imagecolorallocate($this->handler, 255, 255, 255);
			$this->handler = imagerotate($this->handler, $arc, $background);
			imagedestroy($this->handler);
			return true;
		}

		public function resize($width = 0, $height = 0) {
			if(empty($this->width) || empty($this->height)) {
				return false;
			}
			if(!$width && !$height) {
				$width  = $this->width;
				$height = $this->height;
			} elseif(!$width && $height > 0) {
				$persents = $height / $this->height;
				$width    = (int)($this->width * $persents);
			} elseif($width > 0 && !$height) {
				$persents = $width / $this->width;
				$height   = (int)($this->height * $persents);
			} else {
				$per_w = $width  / $this->width;
				$per_h = $height / $this->height;
				if($per_w < $per_h) {
					$height = (int)($this->height * $per_w);
				} else {
					$width  = (int)($this->width * $per_h);
				}
			}
			
			$tmpImage	= ImageCreateTrueColor($width, $height);
			$white		= imagecolorallocate($tmpImage, 255, 255, 255);
			imagefilledrectangle($tmpImage, 0, 0, $width, $height, $white);
			
			if(!imagecopyresampled($tmpImage, $this->handler, 0,0,0,0,$width,$height,$this->width,$this->height)) {
				return false;
			}
			
			$this->handler	= $tmpImage;
			$this->width	= $width;
			$this->height	= $height;
			return true;
		}

		public function display() {
			header('Content-type: image/jpeg');
			imageJPEG($this->handler);
			imagedestroy($this->handler);
		}

		public function save($file, $quality = 90) {
			if(empty($this->handler)) {
				return false;
			}
			imageJPEG($this->handler, $file, $quality);
		}
		
		function cutToSize($width = false, $height = false) {
			if(!$width && !$height) return false;
			if(!$width || !$height) {
				$this->resize($width, $height);
				return true;
			}
			
			// определение по какому параметру подгонять
			$persents = $width / $this->width;
			if($persents * $this->height > $height) {
				$this->resize($width);
				$tmpImage = ImageCreateTrueColor($width, $height);
				if(!imagecopyresampled($tmpImage,$this->handler,0,0,0,(int)($persents*$this->height-$height)/2,$width,$height,$width,$height)) {
					return false;
				}
				$this->handler	= $tmpImage;
				$this->width	= $width;
				$this->height	= $height;
				
				return true;
			}
			$this->resize(0,$height);
			$persents=$height/$this->height;
			$tmpImage = ImageCreateTrueColor($width,$height);
			if(!imagecopyresampled($tmpImage,$this->handler,0,0,(int)(($persents*$this->width-$width)/2),0,$width,$height,$width,$height)) {
				return false;
			}
			$this->handler	= $tmpImage;
			$this->width	= $width;
			$this->height	= $height;
			
			return true;
		}

		public function reset() {
			if(!empty($this->handler) && !empty($this->image_path)) {
				imagedestroy($this->handler);
				$this->handler = null;
				$this->open($this->image_path);
			}
		}

		public function addWatermark($watermarkPath) {
			$watermark       = imageCreateFromPng($watermarkPath);
			$watermarkWidth  = imageSX($watermark);
			$watermarkHeight = imageSY($watermark);
			ImageCopyResampled($this->handler,$watermark,(int) ($this->width-($watermarkWidth + 10)),(int)($this->height-($watermarkHeight + 10)),0,0,$watermarkWidth,$watermarkHeight,$watermarkWidth,$watermarkHeight);
		}
		
	}
?>