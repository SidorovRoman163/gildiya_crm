<?php
	class date_component extends core_component {
		
		public function month($month, $is_named = true) {
			switch((int)$month) {
				case  1: return $is_named ? 'январь'	: 'января';
				case  2: return $is_named ? 'февраль'	: 'февраля';
				case  3: return $is_named ? 'март'		: 'марта';
				case  4: return $is_named ? 'апрель'	: 'апреля';
				case  5: return $is_named ? 'май'		: 'мая';
				case  6: return $is_named ? 'июнь'		: 'июня';
				case  7: return $is_named ? 'июль'		: 'июля';
				case  8: return $is_named ? 'август'	: 'августа';
				case  9: return $is_named ? 'сентябрь'	: 'сентября';
				case 10: return $is_named ? 'октябрь'	: 'октября';
				case 11: return $is_named ? 'ноябрь'	: 'ноября';
				case 12: return $is_named ? 'декабрь'	: 'декабря';
			}
		}
		
		public function weekday($ts) {
			switch((int)date('N', $ts)) {
				case 1: return 'пн';
				case 2: return 'вт';
				case 3: return 'ср';
				case 4: return 'чт';
				case 5: return 'пт';
				case 6: return 'сб';
				case 7: return 'вс';
			}
			return '';
		}
		
		public function common($ts) {
			if(!is_numeric($ts)) {
				$ts = strtotime($ts);
			}
		
			return join(' ', array(
				date('d', $ts),
				$this->month(date('m', $ts), false),
				date('Y', $ts)
			));
		}
		
		
		public function monthUpFirst($month, $is_named = true) {
			return mb_convert_case($this->month($month, $is_named), MB_CASE_TITLE, "UTF-8");
		}
		
		public function commentFormat($ts) {
			if(!is_numeric($ts)) $ts = strtotime($ts);
			
			$result = '';
			$diff_today_start	= $ts - strtotime(date('Y-m-d 00:00:00'));
			$diff_today_start  /= 86400;
			
			if($diff_today_start >= -2 && $diff_today_start < 1) {
				if($diff_today_start >= 0) {
					$result = 'сегодня';
				} elseif($diff_today_start >= -1) {
					$result = 'вчера';
				} elseif($diff_today_start >= -2) {
					$result = 'позавчера';
				}
			} else {
				$result = date('d', $ts).' '.$this->month(date('m', $ts), false);
				if(date('Y') != date('Y', $ts)) {
					$result .= ' '.date('Y', $ts);
				}
			}
			
			return $result.', '.date('H:i', $ts);
		}
		
		public function accountFormat($timestamp) {
			return date('d '.$this->monthUpFirst(date('m', $timestamp), false).' Y г.', $timestamp);
		}
		
	}
?>