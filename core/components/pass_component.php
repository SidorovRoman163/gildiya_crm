<?php
	class pass_component extends core_component {		
	
		private $symbol_array;
		private $pass;
		
		public function __init() {
			$this->symbol_array = "qwertyuipasdfghjklzxcvbnmQWERTYUIPASDFGHJKLZXCVBNM123456789";			
		}
		
		public function get_pass($length = 6) {
			$max = strlen($this->symbol_array) - 1;
			for($i = 0; $i < $length; $i++){				
				$this->pass .= $this->symbol_array[rand(0, $max)];				
			}			
			return $this->pass;
		}	
	}
?>