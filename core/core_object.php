<?php
	class core_object {
	
		public static $classes				= array();  // массив загруженных классов
		public static $controller			= null;		// ссылка на объект контролера
		public static $app_name				= 'app';	// наименование приложения
		
		public static $lang_id				= 0;		// ID языка
		public static $lang					= '';		// Название языка
		public static $is_ajax				= false;	// флаг заголовка XMLHttpRequest
		public static $uri_controller		= 'main';	// имя вызываемого контроллера
		public static $uri_method			= 'index';	// имя вызываемого метода
		public static $uri_params			= array();	// массив параметров
		
		public static $pref					= '';
		public static $files				= array();	// глобальный массив $_FILES
		public static $post					= array();	// глобальный массив $_POST
		public static $get					= array();	// глобальный массив $_GET
		
		private static $is_init				= false;
		
		final public function __construct() {
			
			if(!self::$is_init) {
				self::$files	= $_FILES;	unset($_FILES);
				self::$post		= $_POST;	unset($_POST);
				self::$get		= $_GET;	unset($_GET);
				
				self::global_arrays_trim(self::$post);
				self::global_arrays_trim(self::$get);
				if(get_magic_quotes_gpc()) {
					self::global_arrays_stripslashes(self::$post);
					self::global_arrays_stripslashes(self::$get);
				}
				
				self::$is_init	= true;
				self::$pref		= $this->_conf->get('db/pref');
			}
			
			$this->__init();
		}
		
		/**
		 * Специальный метод, выполняющий роль конструктора для потомков
		 */
		protected function __init() { }
		
		/**
		 * "Волшебный метод", выполняющийся в случае вызова нексуществующего поля класса,
		 * является основным и единственным методом межклассового взаимодействия
		 */
		public function __get($name = '') {
			if(!empty(self::$classes[$name])) {
				return self::$classes[$name];
			}
			
			// восстанавливаем название класса
			$class_name = $name.'_model';
			if($name[0] == '_') {
				$class_name = substr($name, 1).'_component';
			} elseif(strpos($name, '_controller')) {
				$class_name = $name;
			}
			
			if(!$class_name || !class_exists($class_name)) {
				return null;
			}
			
			self::$classes[$name] = new $class_name();
			return self::$classes[$name];
		}
		
		/**
		 * Геттер для получения данных из глобального массива $_POST
		 */
		public static function _post($label) {
			return (isset(self::$post[$label])) ? self::$post[$label] : null;
		}
		
		/**
		 * Геттер для получения данных из глобального массива $_GET
		 */
		public static function _get($label) {
			return (isset(self::$get[$label])) ? self::$get[$label] : null;
		}
		
		/**
		 * Геттер для получения данных из глобального массива $_FILES
		 */
		public static function _files($label) {
			return (isset(self::$files[$label])) ? self::$files[$label] : null;
		}
		
		/** 
		 * Рекурсивная обработка элементов массива функцией trim()
		 */
		private static function global_arrays_trim(&$value) {
			if(is_array($value)) {
				foreach($value as $i => &$item) {
					self::global_arrays_trim($item);
				}
				return true;
			}
			$value = trim($value);
		}
		
		/** 
		 * Рекурсивная обработка элементов массива функцией stripslashes()
		 */
		private static function global_arrays_stripslashes(&$value) {
			if(is_array($value)) {
				foreach($value as $i => &$item) {
					self::global_arrays_stripslashes($item);
				}
				return true;
			}
			$value = stripslashes($value);
		}
	
	}
?>