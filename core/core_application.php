<?php
	/**
	 * Универсальный класс приложения.
	 *
	 * Разбивает URL-адрес на состовляющие, настраивает окружение,
	 * запускает класс-делегат конкретного приложения (публичный сайт или система управления).
	 */
	class core_application extends core_object {

		private static $uri_parts = array(); // массив частей URI-адреса
		
		/**
		 * Комплексное выполнение задач класса
		 */
		public static function start() {
			
			// определение заголовка XMLHttpRequest
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
				self::$is_ajax = true;
			}
			
			// парсинг URL, определение приложения, делегата, контроллера и т.п.
			if(!empty($_GET['controller']) && !empty($_GET['method'])) {
				$url_parts = self::uri_parse('/'.$_GET['controller'].'/'.$_GET['method'].'/');
			} else {
				$url_parts	 = self::uri_parse($_SERVER['REQUEST_URI']);
			}
			
			$application			= self::$app_name.'_application';
			$controller				= 'main_controller';

			if(self::uri_get_first()) {
				self::$uri_controller	= self::uri_get_first();
				$controller				= self::$uri_controller.'_controller';
			}
			if(class_exists($controller)) {
				self::uri_clear_first();
			} elseif(class_exists('error_controller')) {
				self::$uri_controller	= 'error';
				self::$uri_method		= 'page_404';
				$controller				= 'error_controller';
			} else {
				$application = new $application();
				return $application->__start();
			}
			
			self::$controller = new $controller();
			if(method_exists(self::$controller, self::uri_get_first())) {
				self::$uri_method = self::uri_get_first();
				self::uri_clear_first();
			} elseif(!method_exists(self::$controller, 'index')) {
				$controller				= 'error_controller';
				self::$uri_controller	= 'error';
				self::$uri_method		= 'page_404';
				self::$controller		= new $controller();
			}
			
			self::$uri_params	= self::$uri_parts;
			self::$uri_parts	= array();
			
			$application = new $application();
			return $application->__start();
		}
		
		/**
		 * Парсинг URI-адреса, заполение массива частей URI
		 */
		private static function uri_parse($uri = '') {
			preg_match('%^/?(.*?)/?((\?|\#|\&).*)?$%i', $uri, $match);
			self::$uri_parts = explode('/', $match[1]);
		}
		
		/**
		 * Отдает первую часть URI-адреса 
		 */
		protected static function uri_get_first() {
			return (!empty(self::$uri_parts[0])) ? self::$uri_parts[0] : false;
		}
		
		/**
		 * Удаляет первую часть URI-адреса, уменьшает массив частей URI
		 */
		protected static function uri_clear_first() {
			array_shift(self::$uri_parts);
		}
	
	}
?>